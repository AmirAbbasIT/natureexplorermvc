﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Enums
    {
        public enum EntityType
        {
            TourType=1,
            HotelType=2
        }
        public enum BookingType
        {
            TourBooking=1,
            HotelBooking=2
        }
        public enum PaymentMethod
        {
            ByCard=1,
            ByEasyPaisa,
            ByCash=3
        }

        public enum NotificationStatus
        {
            Unread=1,
            Read=2
        }
    }
}
