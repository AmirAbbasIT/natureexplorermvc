﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Hotel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        
        [ForeignKey("Picture")]
        public int PictureID { get; set; }
        public Picture Picture { get; set; }

        public virtual List<HotelPackage> HotelPackages { get; set; }
        public virtual List<HotelFacility> HotelFacilities { get; set; }
        public Hotel()
        {
            HotelPackages = new List<HotelPackage>();
            HotelFacilities = new List<HotelFacility>();
            Picture = new Picture();
        }
    }
}
