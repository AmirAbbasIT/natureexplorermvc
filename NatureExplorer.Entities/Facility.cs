﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Facility
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
