﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Like
    {
        public int ID { get; set; }
        public int EntityID { get; set; }
        public int RecordID { get; set; }
        public string UserID { get; set; }
        public virtual NEUser User { get; set; }
    }
}
