﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class RoomReservation
    {
        public int ID { get; set; }
        public int Persons { get; set; }
        public decimal Price { get; set; }
        public string UserID { get; set; }
        public NEUser User { get; set; }
        public DateTime Date { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }

        public int HotelID { get; set; }

        public int HotelPackageID { get; set; }

        public int PaymentMethodType { get; set; }
        public int PaymentTransectionID { get; set; }
        public bool Processed { get; set; }


    }
}
