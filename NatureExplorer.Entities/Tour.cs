﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Tour
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime DepartureDate { get; set; }
        public int Days { get; set; }
        public int Nights { get; set; }
        public string Destination { get; set; }
        public virtual List<TourPicture> Pictures { get; set; }
        public virtual List<TourDayDetail> TourDays { get; set; }
        public virtual List<TourNightDetail> TourNights { get; set; }
        public virtual List<TourPricing> TourPrices { get; set; }
        public virtual List<TourFacility> TourFacilities { get; set; }
        public virtual List<TourBus> TourBuses { get; set; }

        public Tour()
        {
            Pictures = new List<TourPicture>();
            TourDays = new List<TourDayDetail>();
            TourNights = new List<TourNightDetail>();
            TourPrices = new List<TourPricing>();
            TourFacilities = new List<TourFacility>();
            TourBuses = new List<TourBus>();
        }
    }
}
