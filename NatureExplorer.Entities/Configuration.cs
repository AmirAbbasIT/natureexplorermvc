﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Configuration
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }

        public string FacebookAppID { get; set; }
        public string FacebookSecretID { get; set; }

        public string GoogleAppID { get; set; }
        public string GoogleSecretID { get; set; }

        public string AuthorizeNetTransectionID { get; set; }
        public string AuthorizeNetAppID { get; set; }

        /// <summary>
        /// QuickLinks
        /// </summary>
        public string FaceBook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public string Contact { get; set; }

        /////
        ///ReccomendationSystem
        ///
        public bool ReccomendationSystem { get; set; }
        public string AppUrl { get; set; }
        public string AdminKey { get; set; }
        public string ReccomendationKey { get; set; }
        public string StorageConnectionString { get; set; }

    }
}
