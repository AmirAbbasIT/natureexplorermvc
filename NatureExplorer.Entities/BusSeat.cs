﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class BusSeat
    {
        public int ID { get; set; }
        public int SeatNo { get; set; }
        public string Status { get; set; }

        public int BookingID { get; set; }
        public int TourBusID { get; set; }
        public virtual TourBus TourBus { get; set; }

        public string UserID { get; set; }
        public virtual NEUser User { get; set; }

    }
}
