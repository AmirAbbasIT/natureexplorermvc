﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class EasyPaisa
    {
        public int ID { get; set; }
        public string TransectionID { get; set; }
        public string Secret { get; set; }
        public string RecievingCNIC { get; set; }

    }
}
