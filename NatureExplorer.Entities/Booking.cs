﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Booking
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string UserID { get; set; }
        public NEUser User { get; set; }
        public int TourBusID { get; set; }
        public List<TourBus> TourBuses { get; set; }
        public int EntityType { get; set; }
        public int RecordID { get; set; }
        public int Persons { get; set; }
        public float Price { get; set; }
        public string Location { get; set; }
        public int PaymentMethodType { get; set; }
        public int PaymentTransectionID { get; set; }
        public bool Processed { get; set; }

        public Booking()
        {
            TourBuses = new List<TourBus>();
        }
    }
}
