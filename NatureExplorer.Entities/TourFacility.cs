﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class TourFacility
    {
        public int ID { get; set; }
        public int TourID { get; set; }
        public int FacilityID { get; set; }
        public virtual Facility Facility { get; set; }
    }
}
