﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class TourPricing
    {
        public int ID { get; set; }
        public int TourID { get; set; }
        public string Location { get; set; }
        public decimal SinglePrice { get; set; }
        public decimal CouplePrice { get; set; }
    }
}
