﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class TourDayDetail
    {
        public int ID { get; set; }
        public int TourID { get; set; }
        public int DayNumber { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }

    }
}
