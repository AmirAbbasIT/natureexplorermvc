﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class BillingInfo
    {
        public int ID { get; set; }
        public string Info { get; set; }
        public bool IsJazzCash { get; set; }
        public bool IsEasyPaisa { get; set; }
    }
}
