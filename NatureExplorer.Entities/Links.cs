﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Links
    {
        public string FaceBook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public string Contact { get; set; }
    }
}
