﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class TourNightDetail
    {
        public int ID { get; set; }
        public int NightNo { get; set; }
        public string HotelToStay { get; set; }

        public int TourID { get; set; }
        //public virtual Tour Tour { get; set; }
        //public TourNightDetail()
        //{
        //    Tour = new Tour();
        //}
    }
}
