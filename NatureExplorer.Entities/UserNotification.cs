﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class UserNotification
    {
        public int ID { get; set; }
        public int Status { get; set; }

        public int NotificationID { get; set; }
        public virtual Notification Notification { get; set; }

        public string UserID { get; set; }
        public virtual NEUser User { get; set; }
    }
}
