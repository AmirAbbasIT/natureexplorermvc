﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Notification
    {
        public int ID { get; set; }
        public string Message { get; set; }
        public DateTime DateAdded { get; set; }
        public string action { get; set; }
        public string controller { get; set; }
        public int resourceID { get; set; }
    }
}
