﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class TourBus
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int TourID { get; set; }
        public virtual Tour Tour { get; set; }

        public virtual List<BusSeat> Seats { get; set; }

        public TourBus()
        {
            Seats = new List<BusSeat>();
        }


    }
}
