﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class NEUser : IdentityUser
    {
        public string FullName { get; set; }
        public string ContantNo { get; set; }
        public string Adress { get; set; }
        public int? PictureID { get; set; }
        public virtual Picture Picture { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<NEUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
