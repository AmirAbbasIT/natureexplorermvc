﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public  class HotelPackage
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }
        public decimal Price { get; set; }
        public int? HotelID { get; set; }
        public virtual Hotel Hotel { get; set; }
        public virtual List<HotelPackagePicture> Pictures { get; set; }

    }
}
