﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Entities
{
    public class Review
    {

        public int ID { get; set; }
        public string Text { get; set; }
        public int Ratings { get; set; }
        public DateTime TimeStamp { get; set; }
        //To Identify Record OF a Particular Record
        public string UserID { get; set; }
        public virtual NEUser User { get; set; }
        public int EntityID { get; set; }
        public int RecordID { get; set; }
    }
}
