﻿using NatureExplorer.Database.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using NatureExplorer.Web.ViewModels;
using Recommendations.Client.Entities;

namespace NatureExplorer.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new HomeVM();
            model.Tours = TourService.GetTourServices().GetAllTours(3);
            model.ReccomendedTours = new List<Tour>();
            foreach (var p in model.Tours)
            {
                p.Pictures = TourService.GetTourServices().GetTourPicturesByTourID(p.ID);
                p.TourPrices = TourService.GetTourServices().GetToursPricingDetail(p.ID);
            }
            if(ConfigurationService.GetConfigurationServices().IsEnabledRecSys())
            {
                var result = ReccomedationService.GetReccomendationServices().GetPersonalizedRecomdendations(new List<UsageEvent>());
                result.OrderBy(r => r.Score);
                for(int i=0;i<3;i++)
                {
                    model.ReccomendedTours.Add(TourService.GetTourServices().GetTour(Int32.Parse(result[i].RecommendedItemId)));
                }
                foreach (var p in model.ReccomendedTours)
                {
                    p.Pictures = TourService.GetTourServices().GetTourPicturesByTourID(p.ID);
                    p.TourPrices = TourService.GetTourServices().GetToursPricingDetail(p.ID);
                }
            }
            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            var model = new StaffVM();
               model.Staff= StaffMemberService.GetStaffServices().GetAllStaff();
            for(int i=0;i<model.Staff.Count;i++)
            {
                model.Pictures.Add(StaffMemberService.GetStaffServices().GetStaffPictureByStaffID(model.Staff[i].PictureID)) ;
            }
            return View(model);
        }

        public ActionResult GetNotifications()
        {
            var userID = User.Identity.GetUserId();
            var model = NotificationService.GetNotificationServices().Get5Notifications(userID);
            return PartialView(model);
        }

        public ActionResult ContactUSForm()
        {
            return PartialView();
        }
        [HttpPost]
        public void AddQuery(Query query)
        {
            query.Date = DateTime.Now;
            Querieservice.GetQuerieservices().AddQuery(query);
        }


        public ActionResult Footer()
        {
            var model = new FooterVM();
            model.Facebook = ConfigurationService.GetConfigurationServices().FaceBook();
            model.Instagram = ConfigurationService.GetConfigurationServices().Instagram();
            model.Twitter = ConfigurationService.GetConfigurationServices().Twitter();
            model.Email = ConfigurationService.GetConfigurationServices().Email();
            model.Contact = ConfigurationService.GetConfigurationServices().Contact();
            return PartialView(model);
        }
    }
}