﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NatureExplorer.Database.Managers;
using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static NatureExplorer.Entities.Enums;

namespace NatureExplorer.Web.Controllers
{
    public class BookingController : Controller
    {
        // GET: Booking
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetSeats(int VehicleNumber)
        {
           var result =BusService.GetBusServices().GetBusSeats(VehicleNumber);
            return Json(new { Data =result });
        }
        [HttpGet]
        public ActionResult BookTour(int ID)
        {
            if (Request.IsAuthenticated)
            {
                var Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
                var model = new ClinetTourBookingVM();
                string userID = User.Identity.GetUserId();
                model.User = Usermanager.FindById(userID);
                model.Tour = TourService.GetTourServices().GetTour(ID);
                model.Tour.TourBuses = BusService.GetBusServices().GetTourBuses(model.Tour.ID);
                model.Booking = new Entities.Booking();
                model.Booking.EntityType = (int)EntityType.TourType;
                model.Booking.RecordID = ID;
                model.BillingInfos = BillingInfoService.GetBillingInfoServices().GetAllBillingInfo();
                return View(model);
            }
            else
            {
                return RedirectToAction("login", "Account", new { returnUrl = HttpContext.Request.Url.GetComponents(UriComponents.PathAndQuery, UriFormat.SafeUnescaped) });
            
            }
        }


        [HttpPost]
        public ActionResult BookTour(ClinetTourBookingVM VM)
        {
            VM.User.PhoneNumber = VM.User.ContantNo;
            //var Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
            //var OrigionalUser = await Usermanager.FindByIdAsync(VM.User.Id);
            //OrigionalUser.Adress = VM.User.Adress;
            //OrigionalUser.FullName = VM.User.FullName;
            //OrigionalUser.ContantNo = VM.User.ContantNo;
            //OrigionalUser.PhoneNumber= VM.User.ContantNo;
            //await Usermanager.UpdateAsync(OrigionalUser);
            //Alternate
            UserService.GetUserServices().UpdateUser(VM.User);
            ///////////////
            VM.Booking.UserID = VM.User.Id;
            VM.Booking.Date = DateTime.Now;
            VM.Booking.PaymentMethodType = VM.PaymentMethod;
            VM.Booking.Price = VM.Total;
            VM.Booking.TourBusID = VM.TourBusID;

            if(VM.PaymentMethod==(int)PaymentMethod.ByCard)///When Payment Method is Credit Card
            {
                var bookingID=BookingService.GetBookingServices().AddBooking(VM.Booking);
                BusService.GetBusServices().BookSeats(VM.User.Id, VM.TourBusID, bookingID, VM.BusSeats.ToArray());
                TempData["card"] = VM.CreditCard;
                TempData["Booking"] = VM.Booking;
                return RedirectToAction("PayCreditCard", "Shared");
     
            }
            else if(VM.PaymentMethod==(int)PaymentMethod.ByEasyPaisa)///When Payment Method is Easy Paisa
            {
                VM.Booking.PaymentTransectionID=PaymentService.GetPaymentServices().AddEasyPaisaTransection(VM.EasyPaisa);
                var bookingID = BookingService.GetBookingServices().AddBooking(VM.Booking);
                BusService.GetBusServices().BookSeats(VM.User.Id, VM.TourBusID, bookingID, VM.BusSeats.ToArray());
            }
            else if(VM.PaymentMethod==(int)PaymentMethod.ByCash)///When Payment Method is Cash in Hand
            {
                var bookingID = BookingService.GetBookingServices().AddBooking(VM.Booking);
                BusService.GetBusServices().BookSeats(VM.User.Id, VM.TourBusID, bookingID, VM.BusSeats.ToArray());
            }
            if (User.Identity.IsAuthenticated)
            {
                string path = Server.MapPath("~/RecSys_Resources/");
                ReccomedationService.GetReccomendationServices().SaveEventtoUsage(User.Identity.GetUserId(), VM.Tour.ID.ToString(), "Purchase", path);
            }
            return RedirectToAction("Index","Home");
        }

        [HttpGet]
        public ActionResult BookRoom(int ID,int? HotelID)
        {
            if(Request.IsAuthenticated)
            {
                var Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
                var model = new ClinetRoomBookingVM();
                string userID = User.Identity.GetUserId();
                model.User = Usermanager.FindById(userID);
                // model.Hotel = TourService.GetTourServices().GetTour(ID);
                model.Package = HotelPackageService.GetHotelPackageServices().GetHotelPackage(ID);
                model.Reservation = new Entities.RoomReservation();
                model.Reservation.HotelPackageID = ID;
                model.Reservation.HotelID = (int)HotelID;
                return View(model);
            }
            else
            {
                return RedirectToAction("login", "Account", new { returnUrl = HttpContext.Request.Url.GetComponents(UriComponents.PathAndQuery, UriFormat.SafeUnescaped) });

            }
            
        }

        [HttpPost]
        public ActionResult BookRoom(ClinetRoomBookingVM VM)
        {
            VM.User.PhoneNumber = VM.User.ContantNo;
            //var Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
            //var OrigionalUser = await Usermanager.FindByIdAsync(VM.User.Id);
            //OrigionalUser.Adress = VM.User.Adress;
            //OrigionalUser.FullName = VM.User.FullName;
            //OrigionalUser.ContantNo = VM.User.ContantNo;
            //OrigionalUser.PhoneNumber = VM.User.ContantNo;
            //await Usermanager.UpdateAsync(OrigionalUser);
            UserService.GetUserServices().UpdateUser(VM.User);
            ///////////////
            VM.Reservation.UserID = VM.User.Id;
            VM.Reservation.Date = DateTime.Now;
            VM.Reservation.PaymentMethodType = VM.PaymentMethod;
            VM.Reservation.Price = (decimal)VM.Total;


            if (VM.PaymentMethod == (int)PaymentMethod.ByCard)///When Payment Method is Credit Card/Debit card
            {
                BookingService.GetBookingServices().AddHotelBooking(VM.Reservation);
                return RedirectToAction("PayCreditCard", "Shared", new { card = VM.CreditCard, bookingInfo = VM.Reservation });
            }
            else if (VM.PaymentMethod == (int)PaymentMethod.ByEasyPaisa)///When Payment Method is Easy Paisa
            {
                VM.Reservation.PaymentTransectionID = PaymentService.GetPaymentServices().AddEasyPaisaTransection(VM.EasyPaisa);
                BookingService.GetBookingServices().AddHotelBooking(VM.Reservation);
            }
            else if (VM.PaymentMethod == (int)PaymentMethod.ByCash)///When Payment Method is Cash in Hand
            {
                BookingService.GetBookingServices().AddHotelBooking(VM.Reservation);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}