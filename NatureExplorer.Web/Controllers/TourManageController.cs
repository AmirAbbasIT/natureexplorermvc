﻿using Microsoft.AspNet.Identity;
using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using NatureExplorer.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static NatureExplorer.Entities.Enums;
using static NatureExplorer.Web.ViewModels.TourVMS;

namespace NatureExplorer.Web.Controllers
{
    public class TourManageController : Controller
    {
        // GET: Tour
        public ActionResult Index(string searchTerm, int? minimumPrice, int? maximumPrice, int? ratings, int? sortBy, int? pageNo = 1, int? pageSize = 6)
        {
            var model = new NatureExplorer.Web.ViewModels.TourVMS.TourVM();
            model.Tours=TourService.GetTourServices().GetAllTours(searchTerm, minimumPrice, maximumPrice, ratings, sortBy, (int)pageNo, (int)pageSize);
            model.Cities = TourService.GetTourServices().GetTourCities();
            model.searchTerm = searchTerm;
            model.minimumPrize = minimumPrice;
            model.MaximumPrize = maximumPrice;
            model.ratings = ratings;
            model.pageNo = pageNo;
            model.pageSize = pageSize;
            model.Pager = new BaseListingVM.Pager(TourService.GetTourServices().TourCount(searchTerm), pageNo, (int)pageSize);
            foreach (var p in model.Tours)
            {
                p.Pictures = TourService.GetTourServices().GetTourPicturesByTourID(p.ID);
                p.TourPrices= TourService.GetTourServices().GetToursPricingDetail(p.ID);
            }
            return View(model);
        }
        public ActionResult Detail(int ID)
        {
            var model = new TourDetailVM();
            model.Tour = TourService.GetTourServices().GetTour(ID);
            model.EntityType = (int)EntityType.TourType;
            model.Tour.Pictures = TourService.GetTourServices().GetTourPicturesByTourID(ID);
            model.Reviews = ReviewService.GetReviewServices().GetReviews((int)EntityType.TourType, ID);
            model.Tour.TourDays = TourService.GetTourServices().GetToursDayDetail(ID);
            model.Tour.TourPrices = TourService.GetTourServices().GetToursPricingDetail(ID);
            model.Tour.TourFacilities = TourService.GetTourServices().GetTourFacilities(ID);
            model.Pictures = TourService.GetTourServices().GetTourPicturesURlByTourID(ID);
           

            if(ConfigurationService.GetConfigurationServices().IsEnabledRecSys())
            {
                ///Getting Reccomended tours
                var reccomendResult = ReccomedationService.GetReccomendationServices().GetItemtoItemRecomdendations(ID.ToString());
                reccomendResult.OrderBy(r => r.Score);
                if (reccomendResult != null && reccomendResult.Count != 0)
                {
                    foreach (var result in reccomendResult)
                    {
                        model.ReccomendedTours.Add(TourService.GetTourServices().GetTour(Int32.Parse(result.RecommendedItemId)));
                    }
                    foreach (var p in model.ReccomendedTours)
                    {
                        p.Pictures = TourService.GetTourServices().GetTourPicturesByTourID(p.ID);
                        p.TourPrices = TourService.GetTourServices().GetToursPricingDetail(p.ID);
                    }
                }
            }       
            if(User.Identity.IsAuthenticated)
            {
                string path = Server.MapPath("~/RecSys_Resources/");
                ReccomedationService.GetReccomendationServices().SaveEventtoUsage(User.Identity.GetUserId(), model.Tour.ID.ToString(), "Click",path);
            }
            return View(model);
        }

        #region PostComment
        [HttpPost]
        public JsonResult PostComment(Review comment)
        {
            comment.TimeStamp = DateTime.Now;
            comment.UserID = User.Identity.GetUserId();
            comment.EntityID =(int)EntityType.TourType;
            comment.Ratings = comment.Ratings;
            ReviewService.GetReviewServices().AddReview(comment);
            JsonResult result = new JsonResult();
            result.Data = new { Success = true };
            return result;
        }
        #endregion
    }
}