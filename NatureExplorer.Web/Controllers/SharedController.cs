﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using NatureExplorer.Database.ServerServices;
using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Controllers
{
    public class SharedController : Controller
    {
        [HttpPost]
        public JsonResult AddPictures()
        {

            var pics = Request.Files;
            JsonResult result = new JsonResult();
            List<object> pictureJson = new List<object>();
            if (pics != null)
            {
                for (int i = 0; i < pics.Count; i++)
                {
                    var picture = pics[i];
                    var fileName = Guid.NewGuid() + Path.GetExtension(picture.FileName);
                    var path = Server.MapPath("~/Content/Images/") + fileName;
                    picture.SaveAs(path);
                    var dbPicture = new Picture();
                    dbPicture.URL = fileName;
                    dbPicture.ID = PictureService.GetPictureServices().AddPicture(fileName);
                    ////StartFrom Here
                    pictureJson.Add(dbPicture);
                }
                result.Data = pictureJson;
            }
            return result;
        }

        [HttpPost]
        public JsonResult DeletePicture(string name)
        {
            string fullPath = Request.MapPath("~/Content/Images/" + name);
            PictureServerService.GetPictureServices().DeletePicture(fullPath);
            return new JsonResult() { };
        }

        public ActionResult PayCreditCard()
        {
            CreditCard card = TempData["card"] as CreditCard;
            Booking bookingInfo = TempData["Booking"] as Booking;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ConfigurationManager.AppSettings["AuthorizNetAPIID"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ConfigurationManager.AppSettings["AuthorizNetTransectionID"],
            };
            var creditCard = new creditCardType
            {
                cardNumber = card.CardNumber,
                expirationDate = card.ExpiryDate,
                cardCode = card.CardCode
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // charge the card
                amount = (decimal)bookingInfo.Price,
                payment = paymentType,
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the controller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            // validate response
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    
                    if (response.transactionResponse.messages != null)
                    {
                        Console.WriteLine("Successfully created transaction with Transaction ID: " + response.transactionResponse.transId);
                        Console.WriteLine("Response Code: " + response.transactionResponse.responseCode);
                        Console.WriteLine("Message Code: " + response.transactionResponse.messages[0].code);
                        Console.WriteLine("Description: " + response.transactionResponse.messages[0].description);
                        Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                    }
                    else
                    {
                        Console.WriteLine("Failed Transaction.");
                        if (response.transactionResponse.errors != null)
                        {
                            Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                            Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                        }
                    }
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    Console.WriteLine("Failed Transaction.");
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                        Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                    }
                    else
                    {
                        Console.WriteLine("Error Code: " + response.messages.message[0].code);
                        Console.WriteLine("Error message: " + response.messages.message[0].text);
                    }
                    return Json(new { Status = "Failed" });
                }
            }
            else
            {
                Console.WriteLine("Null Response.");
                return Json(new { Status = "Failed" });
            }

        }
    }
}