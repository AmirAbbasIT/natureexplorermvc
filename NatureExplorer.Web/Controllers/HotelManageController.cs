﻿using Microsoft.AspNet.Identity;
using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static NatureExplorer.Entities.Enums;
using static NatureExplorer.Web.ViewModels.HotelVMS;

namespace NatureExplorer.Web.Controllers
{
    public class HotelManageController : Controller
    {
        // GET: Hotel
        public ActionResult Index(string searchTerm, int? minimumPrice, int? maximumPrice, int? ratings, int? sortBy, int? pageNo=1,int? pageSize=3)
        {
            var model = new HotelVM();
            model.Hotels = HotelService.GetHotelServices().GetAllHotels(searchTerm,minimumPrice,maximumPrice,ratings,sortBy,(int)pageNo,(int)pageSize);
            model.searchTerm = searchTerm;
            model.minimumPrize = minimumPrice;
            model.MaximumPrize = maximumPrice;
            model.ratings = ratings;
            model.pageNo = pageNo;
            model.pageSize = pageSize;
            model.Pager = new BaseListingVM.Pager(HotelService.GetHotelServices().HotelCount(searchTerm), pageNo, (int)pageSize);
            foreach(var hotel in model.Hotels)
            {
                hotel.Picture = HotelService.GetHotelServices().GetHotelPictureByHotelID(hotel.PictureID);
            }
            return View(model);
        }
        public ActionResult Detail(int ID)
        {
            var model = new HotelDetailVM();
            model.EntityType = (int)EntityType.HotelType;
            model.Hotel = HotelService.GetHotelServices().GetHotel(ID);
            model.Reviews = ReviewService.GetReviewServices().GetReviews((int)EntityType.HotelType, ID);
            model.Packages = HotelService.GetHotelServices().GetHotelPackages(ID);
            model.Pictures = new List<Entities.HotelPackagePicture>();
            foreach(var p in model.Packages)
            {
                p.Pictures = HotelPackageService.GetHotelPackageServices().GetHotelPackagePicturesByHotelPackageID(p.ID);
            }
            
            return View(model);
        }
        #region PostComment
        [HttpPost]
        public JsonResult PostComment(Review comment)
        {
            comment.TimeStamp = DateTime.Now;
            comment.UserID = User.Identity.GetUserId();
            comment.EntityID = (int)EntityType.HotelType;
            comment.Ratings = comment.Ratings;
            ReviewService.GetReviewServices().AddReview(comment);
            JsonResult result = new JsonResult();
            result.Data = new { Success = true };
            return result;
        }
        #endregion
    }
}