﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NatureExplorer.Web.Startup))]
namespace NatureExplorer.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
