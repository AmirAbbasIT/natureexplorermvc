﻿var HiAceSeats = {
    "type": "FeatureCollection", "features": [
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 188], [50, 208], [70, 208], [70, 188], [50, 188]]], [[[50, 210], [50, 215], [70, 215], [70, 210], [50, 210]]]] }, "properties": { "seatNo": 1, "fill": "Orange" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 188], [75, 208], [95, 208], [95, 188], [75, 188]]], [[[75, 210], [75, 215], [95, 215], [95, 210], [75, 210]]]] }, "properties": { "seatNo": 2, "fill": "Orange" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 141], [0, 161], [20, 161], [20, 141], [0, 141]]], [[[0, 163], [0, 168], [20, 168], [20, 163], [0, 163]]]] }, "properties": { "seatNo": 3, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 141], [25, 161], [45, 161], [45, 141], [25, 141]]], [[[25, 163], [25, 168], [45, 168], [45, 163], [25, 163]]]] }, "properties": { "seatNo": 4, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 141], [50, 161], [70, 161], [70, 141], [50, 141]]], [[[50, 163], [50, 168], [70, 168], [70, 163], [50, 163]]]] }, "properties": { "seatNo": 5, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 141], [75, 161], [95, 161], [95, 141], [75, 141]]], [[[75, 163], [75, 168], [95, 168], [95, 163], [75, 163]]]] }, "properties": { "seatNo": 6, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 94], [0, 114], [20, 114], [20, 94], [0, 94]]], [[[0, 116], [0, 121], [20, 121], [20, 116], [0, 116]]]] }, "properties": { "seatNo": 7, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 94], [25, 114], [45, 114], [45, 94], [25, 94]]], [[[25, 116], [25, 121], [45, 121], [45, 116], [25, 116]]]] }, "properties": { "seatNo": 8, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 94], [50, 114], [70, 114], [70, 94], [50, 94]]], [[[50, 116], [50, 121], [70, 121], [70, 116], [50, 116]]]] }, "properties": { "seatNo": 9, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 94], [75, 114], [95, 114], [95, 94], [75, 94]]], [[[75, 116], [75, 121], [95, 121], [95, 116], [75, 116]]]] }, "properties": { "seatNo": 10, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 47], [0, 67], [20, 67], [20, 47], [0, 47]]], [[[0, 69], [0, 74], [20, 74], [20, 69], [0, 69]]]] }, "properties": { "seatNo": 11, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 47], [25, 67], [45, 67], [45, 47], [25, 47]]], [[[25, 69], [25, 74], [45, 74], [45, 69], [25, 69]]]] }, "properties": { "seatNo": 12, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 47], [50, 67], [70, 67], [70, 47], [50, 47]]], [[[50, 69], [50, 74], [70, 74], [70, 69], [50, 69]]]] }, "properties": { "seatNo": 13, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 47], [75, 67], [95, 67], [95, 47], [75, 47]]], [[[75, 69], [75, 74], [95, 74], [95, 69], [75, 69]]]] }, "properties": { "seatNo": 14, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 0], [0, 20], [20, 20], [20, 0], [0, 0]]], [[[0, 22], [0, 27], [20, 27], [20, 22], [0, 22]]]] }, "properties": { "seatNo": 15, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 0], [25, 20], [45, 20], [45, 0], [25, 0]]], [[[25, 22], [25, 27], [45, 27], [45, 22], [25, 22]]]] }, "properties": { "seatNo": 16, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 0], [50, 20], [70, 20], [70, 0], [50, 0]]], [[[50, 22], [50, 27], [70, 27], [70, 22], [50, 22]]]] }, "properties": { "seatNo": 17, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 0], [75, 20], [95, 20], [95, 0], [75, 0]]], [[[75, 22], [75, 27], [95, 27], [95, 22], [75, 22]]]] }, "properties": { "seatNo": 18, "fill": "grey" } }
    ]
};


//Unsorted and origional Array....
//var HiAceSeats = {
//    "type": "FeatureCollection", "features": [
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 0], [0, 20], [20, 20], [20, 0], [0, 0]]], [[[0, 22], [0, 27], [20, 27], [20, 22], [0, 22]]]] }, "properties": { "seatNo": 15, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 47], [0, 67], [20, 67], [20, 47], [0, 47]]], [[[0, 69], [0, 74], [20, 74], [20, 69], [0, 69]]]] }, "properties": { "seatNo": 11, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 94], [0, 114], [20, 114], [20, 94], [0, 94]]], [[[0, 116], [0, 121], [20, 121], [20, 116], [0, 116]]]] }, "properties": { "seatNo": 7, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 141], [0, 161], [20, 161], [20, 141], [0, 141]]], [[[0, 163], [0, 168], [20, 168], [20, 163], [0, 163]]]] }, "properties": { "seatNo": 3, "fill": "grey" } },

//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 0], [25, 20], [45, 20], [45, 0], [25, 0]]], [[[25, 22], [25, 27], [45, 27], [45, 22], [25, 22]]]] }, "properties": { "seatNo": 16, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 47], [25, 67], [45, 67], [45, 47], [25, 47]]], [[[25, 69], [25, 74], [45, 74], [45, 69], [25, 69]]]] }, "properties": { "seatNo": 12, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 94], [25, 114], [45, 114], [45, 94], [25, 94]]], [[[25, 116], [25, 121], [45, 121], [45, 116], [25, 116]]]] }, "properties": { "seatNo": 8, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 141], [25, 161], [45, 161], [45, 141], [25, 141]]], [[[25, 163], [25, 168], [45, 168], [45, 163], [25, 163]]]] }, "properties": { "seatNo": 4, "fill": "grey" } },

//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 0], [50, 20], [70, 20], [70, 0], [50, 0]]], [[[50, 22], [50, 27], [70, 27], [70, 22], [50, 22]]]] }, "properties": { "seatNo": 17, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 47], [50, 67], [70, 67], [70, 47], [50, 47]]], [[[50, 69], [50, 74], [70, 74], [70, 69], [50, 69]]]] }, "properties": { "seatNo": 13, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 94], [50, 114], [70, 114], [70, 94], [50, 94]]], [[[50, 116], [50, 121], [70, 121], [70, 116], [50, 116]]]] }, "properties": { "seatNo": 9, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 141], [50, 161], [70, 161], [70, 141], [50, 141]]], [[[50, 163], [50, 168], [70, 168], [70, 163], [50, 163]]]] }, "properties": { "seatNo": 5, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 188], [50, 208], [70, 208], [70, 188], [50, 188]]], [[[50, 210], [50, 215], [70, 215], [70, 210], [50, 210]]]] }, "properties": { "seatNo": 1, "fill": "Orange" } },

//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 0], [75, 20], [95, 20], [95, 0], [75, 0]]], [[[75, 22], [75, 27], [95, 27], [95, 22], [75, 22]]]] }, "properties": { "seatNo": 18, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 47], [75, 67], [95, 67], [95, 47], [75, 47]]], [[[75, 69], [75, 74], [95, 74], [95, 69], [75, 69]]]] }, "properties": { "seatNo": 14, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 94], [75, 114], [95, 114], [95, 94], [75, 94]]], [[[75, 116], [75, 121], [95, 121], [95, 116], [75, 116]]]] }, "properties": { "seatNo": 10, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 141], [75, 161], [95, 161], [95, 141], [75, 141]]], [[[75, 163], [75, 168], [95, 168], [95, 163], [75, 163]]]] }, "properties": { "seatNo": 6, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 188], [75, 208], [95, 208], [95, 188], [75, 188]]], [[[75, 210], [75, 215], [95, 215], [95, 210], [75, 210]]]] }, "properties": { "seatNo": 2, "fill": "Orange" } }
//    ]
//};