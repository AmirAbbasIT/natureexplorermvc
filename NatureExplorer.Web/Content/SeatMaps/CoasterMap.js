﻿var CoasterSeats = {
    "type": "FeatureCollection", "features": [
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 222], [75, 242], [95, 242], [95, 222], [75, 222]]], [[[75, 244], [75, 249], [95, 249], [95, 244], [75, 244]]]] }, "properties": { "seatNo": 1, "fill": "Orange" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 185], [0, 205], [20, 205], [20, 185], [0, 185]]], [[[0, 207], [0, 212], [20, 212], [20, 207], [0, 207]]]] }, "properties": { "seatNo": 2, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 185], [25, 205], [45, 205], [45, 185], [25, 185]]], [[[25, 207], [25, 212], [45, 212], [45, 207], [25, 207]]]] }, "properties": { "seatNo": 3, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 185], [75, 205], [95, 205], [95, 185], [75, 185]]], [[[75, 207], [75, 212], [95, 212], [95, 207], [75, 207]]]] }, "properties": { "seatNo": 4, "fill": "Orange" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 148], [0, 168], [20, 168], [20, 148], [0, 148]]], [[[0, 170], [0, 175], [20, 175], [20, 170], [0, 170]]]] }, "properties": { "seatNo": 5, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 148], [25, 168], [45, 168], [45, 148], [25, 148]]], [[[25, 170], [25, 175], [45, 175], [45, 170], [25, 170]]]] }, "properties": { "seatNo": 6, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 111], [0, 131], [20, 131], [20, 111], [0, 111]]], [[[0, 133], [0, 138], [20, 138], [20, 133], [0, 133]]]] }, "properties": { "seatNo": 7, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 111], [25, 131], [45, 131], [45, 111], [25, 111]]], [[[25, 133], [25, 138], [45, 138], [45, 133], [25, 133]]]] }, "properties": { "seatNo": 8, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 111], [75, 131], [95, 131], [95, 111], [75, 111]]], [[[75, 133], [75, 138], [95, 138], [95, 133], [75, 133]]]] }, "properties": { "seatNo": 9, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 74], [0, 94], [20, 94], [20, 74], [0, 74]]], [[[0, 96], [0, 101], [20, 101], [20, 96], [0, 96]]]] }, "properties": { "seatNo": 10, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 74], [25, 94], [45, 94], [45, 74], [25, 74]]], [[[25, 06], [25, 101], [45, 101], [45, 96], [25, 96]]]] }, "properties": { "seatNo": 11, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 74], [75, 94], [95, 94], [95, 74], [75, 74]]], [[[75, 96], [75, 101], [95, 101], [95, 96], [75, 96]]]] }, "properties": { "seatNo": 12, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 37], [0, 57], [20, 57], [20, 37], [0, 37]]], [[[0, 59], [0, 64], [20, 64], [20, 59], [0, 59]]]] }, "properties": { "seatNo": 13, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 37], [25, 57], [45, 57], [45, 37], [25, 37]]], [[[25, 59], [25, 64], [45, 64], [45, 59], [25, 59]]]] }, "properties": { "seatNo": 14, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 37], [75, 57], [95, 57], [95, 37], [75, 37]]], [[[75, 59], [75, 64], [95, 64], [95, 59], [75, 59]]]] }, "properties": { "seatNo": 15, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 0], [0, 20], [20, 20], [20, 0], [0, 0]]], [[[0, 22], [0, 27], [20, 27], [20, 22], [0, 22]]]] }, "properties": { "seatNo": 16, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 0], [25, 20], [45, 20], [45, 0], [25, 0]]], [[[25, 22], [25, 27], [45, 27], [45, 22], [25, 22]]]] }, "properties": { "seatNo": 17, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 0], [50, 20], [70, 20], [70, 0], [50, 0]]], [[[50, 22], [50, 27], [70, 27], [70, 22], [50, 22]]]] }, "properties": { "seatNo": 18, "fill": "grey" } },
        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 0], [75, 20], [95, 20], [95, 0], [75, 0]]], [[[75, 22], [75, 27], [95, 27], [95, 22], [75, 22]]]] }, "properties": { "seatNo": 19, "fill": "grey" } }
    ]
};

//var CoasterSeats = {
//    "type": "FeatureCollection", "features": [
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 0], [0, 20], [20, 20], [20, 0], [0, 0]]], [[[0, 22], [0, 27], [20, 27], [20, 22], [0, 22]]]] }, "properties": { "seatNo": 16, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 37], [0, 57], [20, 57], [20, 37], [0, 37]]], [[[0, 59], [0, 64], [20, 64], [20, 59], [0, 59]]]] }, "properties": { "seatNo": 13, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 74], [0, 94], [20, 94], [20, 74], [0, 74]]], [[[0, 96], [0, 101], [20, 101], [20, 96], [0, 96]]]] }, "properties": { "seatNo": 10, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 111], [0, 131], [20, 131], [20, 111], [0, 111]]], [[[0, 133], [0, 138], [20, 138], [20, 133], [0, 133]]]] }, "properties": { "seatNo": 7, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 148], [0, 168], [20, 168], [20, 148], [0, 148]]], [[[0, 170], [0, 175], [20, 175], [20, 170], [0, 170]]]] }, "properties": { "seatNo": 5, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[0, 185], [0, 205], [20, 205], [20, 185], [0, 185]]], [[[0, 207], [0, 212], [20, 212], [20, 207], [0, 207]]]] }, "properties": { "seatNo": 2, "fill": "grey" } },

//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 0], [25, 20], [45, 20], [45, 0], [25, 0]]], [[[25, 22], [25, 27], [45, 27], [45, 22], [25, 22]]]] }, "properties": { "seatNo": 17, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 37], [25, 57], [45, 57], [45, 37], [25, 37]]], [[[25, 59], [25, 64], [45, 64], [45, 59], [25, 59]]]] }, "properties": { "seatNo": 14, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 74], [25, 94], [45, 94], [45, 74], [25, 74]]], [[[25, 06], [25, 101], [45, 101], [45, 96], [25, 96]]]] }, "properties": { "seatNo": 11, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 111], [25, 131], [45, 131], [45, 111], [25, 111]]], [[[25, 133], [25, 138], [45, 138], [45, 133], [25, 133]]]] }, "properties": { "seatNo": 8, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 148], [25, 168], [45, 168], [45, 148], [25, 148]]], [[[25, 170], [25, 175], [45, 175], [45, 170], [25, 170]]]] }, "properties": { "seatNo": 6, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[25, 185], [25, 205], [45, 205], [45, 185], [25, 185]]], [[[25, 207], [25, 212], [45, 212], [45, 207], [25, 207]]]] }, "properties": { "seatNo": 3, "fill": "grey" } },

//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[50, 0], [50, 20], [70, 20], [70, 0], [50, 0]]], [[[50, 22], [50, 27], [70, 27], [70, 22], [50, 22]]]] }, "properties": { "seatNo": 18, "fill": "grey" } },

//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 0], [75, 20], [95, 20], [95, 0], [75, 0]]], [[[75, 22], [75, 27], [95, 27], [95, 22], [75, 22]]]] }, "properties": { "seatNo": 19, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 37], [75, 57], [95, 57], [95, 37], [75, 37]]], [[[75, 59], [75, 64], [95, 64], [95, 59], [75, 59]]]] }, "properties": { "seatNo": 15, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 74], [75, 94], [95, 94], [95, 74], [75, 74]]], [[[75, 96], [75, 101], [95, 101], [95, 96], [75, 96]]]] }, "properties": { "seatNo": 12, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 111], [75, 131], [95, 131], [95, 111], [75, 111]]], [[[75, 133], [75, 138], [95, 138], [95, 133], [75, 133]]]] }, "properties": { "seatNo": 9, "fill": "grey" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 185], [75, 205], [95, 205], [95, 185], [75, 185]]], [[[75, 207], [75, 212], [95, 212], [95, 207], [75, 207]]]] }, "properties": { "seatNo": 4, "fill": "Orange" } },
//        { "type": "Feature", "geometry": { "type": "MultiPolygon", "coordinates": [[[[75, 222], [75, 242], [95, 242], [95, 222], [75, 222]]], [[[75, 244], [75, 249], [95, 249], [95, 244], [75, 244]]]] }, "properties": { "seatNo": 1, "fill": "Orange" } }

//    ]
//};