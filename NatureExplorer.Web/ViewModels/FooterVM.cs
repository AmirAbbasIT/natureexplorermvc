﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Web.ViewModels
{
    public class FooterVM
    {
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Contact { get; set; }
        public string Twitter { get; set; }
        public string Email { get; set; }

    }
}