﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.ViewModels
{
    public class TourVMS
    {
        public class TourVM
        {
            public int? pageNo { get; set; }
            public int? pageSize { get; set; }
            public int? minimumPrize { get; set; }
            public int? MaximumPrize { get; set; }
            public int? ratings { get; set; }
            public string searchTerm { get; set; }
            public List<Tour> Tours { get; set; }
            public List<String> Cities { get; set; }
            public List<Facility> Facilities { get; set; }
            public Pager Pager { get; set; }
        }
        public class TourDetailVM
        {
            public Tour Tour { get; set; }
            public int EntityType { get; set; }
            public List<Review> Reviews { get; set; }
            public List<String> Pictures { get; set; }

            public List<Tour> ReccomendedTours { get; set; }
            public TourDetailVM()
            {
                ReccomendedTours = new List<Tour>();
            }
        }
    }
}