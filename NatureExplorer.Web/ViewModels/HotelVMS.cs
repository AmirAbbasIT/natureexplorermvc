﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.ViewModels
{
    public class HotelVMS
    {
        public class HotelVM
        {
            public int? pageNo { get; set; }
            public int? pageSize { get; set; }
            public int? minimumPrize { get; set; }
            public int? MaximumPrize { get; set; }
            public int? ratings { get; set; }
            public string searchTerm { get; set; }
            public List<Hotel> Hotels { get; set; }
            public List<Facility> Facilities { get; set; }
            public Pager Pager { get; set; }
        }
        public class HotelDetailVM
        {
            public int EntityType { get; set; }
            public List<Review> Reviews { get; set; }
            public Hotel Hotel { get; set; }
            public List<HotelPackage> Packages { get; set; }
            public List<HotelPackagePicture> Pictures { get; set; }
        }
    }
}