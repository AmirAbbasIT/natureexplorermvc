﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Web.ViewModels
{
    public class ClinetTourBookingVM
    {
        public NEUser User { get; set; }
        public EasyPaisa EasyPaisa { get; set; }
        public CreditCard CreditCard { get; set; }
        public Tour Tour { get; set; }
        public Booking Booking { get; set; }
        public float Total { get; set; }
        public int PaymentMethod { get; set; }
        public int TourBusID { get; set; }
        public List<int> BusSeats { get; set; }
        public List<BillingInfo> BillingInfos { get; set; }
    }

    public class ClinetRoomBookingVM
    {
        public NEUser User { get; set; }
        public EasyPaisa EasyPaisa { get; set; }
        public CreditCard CreditCard { get; set; }
        public Hotel Hotel { get; set; }
        public HotelPackage Package { get; set; }
        public RoomReservation Reservation { get; set; }
        public float Total { get; set; }
        public int PaymentMethod { get; set; }
    }
}