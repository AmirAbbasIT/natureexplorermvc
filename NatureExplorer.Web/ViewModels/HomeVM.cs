﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Web.ViewModels
{
    public class HomeVM
    {
        public List<Tour> Tours { get; set; }
        public List<Tour> ReccomendedTours { get; set; }

    }
}