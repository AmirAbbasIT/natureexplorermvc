﻿using Microsoft.AspNet.Identity.EntityFramework;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class UsersVM
    {
        public List<NEUser> users { get; set; }
        public List<IdentityRole> roles { get; set; }
        public string searchTerm { get; set; }
        public string roleTerm { get; set; }
        public Pager pager { get; set; }
    }
}