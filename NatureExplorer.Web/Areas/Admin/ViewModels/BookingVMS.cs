﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class TourBookingVM
    {
        public List<Booking> bookings { get; set; }
        public string searchTerm { get; set; }
        public Pager pager { get; set; }
    }
    public class TourBookingViewVM
    {
        public Booking Booking { get; set; }
        public EasyPaisa EasyPaisa { get; set; }
        public Tour Tour { get; set; }
        public TourBus TourBus { get; set; }
        public List<BusSeat> Seats { get; set; }
        public Pager pager { get; set; }
    }
    public class HotelBookingVM
    {
        public List<RoomReservation> bookings { get; set; }
        public string searchTerm { get; set; }
        public Pager pager { get; set; }
    }
    public class HotelBookingViewVM
    {
        public RoomReservation Booking { get; set; }
        public EasyPaisa EasyPaisa { get; set; }
        public Hotel Hotel { get; set; }
        public HotelPackage HotelPackage { get; set; }

    }
    //public class ADDTourVM
    //{
    //    public List<Facility> facilities { get; set; }

    //}
    //public class EditTourVM
    //{
    //    public Tour Tour { get; set; }
    //    public List<Facility> facilities { get; set; }

    //}
}