﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class UserDetailVM
    {
        public NEUser User { get; set; }
    }
}