﻿using Microsoft.AspNet.Identity.EntityFramework;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class UserRolesDetailVM
    {
        public NEUser User { get; set; }
        public List<IdentityRole> AvailableRoles { get; set; }
        public List<IdentityRole> UserRoles { get; set; }
    }
}