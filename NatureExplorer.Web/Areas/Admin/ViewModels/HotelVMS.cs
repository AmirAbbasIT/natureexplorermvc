﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class HotelVMS
    {
        public class HotelVM
        {
            public List<Hotel> Hotels { get; set; }
            public string searchTerm { get; set; }
            public Pager pager { get; set; }
        }
        public class ADDHotelVM
        {
            public List<Facility> facilities { get; set; }

        }
        public class EditHotelVM
        {
            public Hotel Hotel { get; set; }
            public List<Facility> facilities { get; set; }

        }
    }
}