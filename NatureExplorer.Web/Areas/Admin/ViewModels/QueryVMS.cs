﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class QueryVM
    {
        public List<Query> queries { get; set; }
        public string searchTerm { get; set; }
        public Pager pager { get; set; }
    }
    public class EditQueryVM
    {
        public Query Query { get; set; }
    }
}