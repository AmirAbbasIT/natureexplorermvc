﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class HotelPackageVMS
    {
        public class HotelPackageVM
        {
            public List<HotelPackage> HotelPackages { get; set; }
            public string searchTerm { get; set; }
            public Pager pager { get; set; }
        }
        public class ADDHotelPackageVM
        {
            public List<Hotel> Hotels { get; set; }

        }
        public class EditHotelPackageVM
        {
            public HotelPackage HotelPackage { get; set; }
            public List<Hotel> Hotels { get; set; }

        }
    }
}