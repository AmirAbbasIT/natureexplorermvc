﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class StaffVM
    {
        public List<Staff> Staff { get; set; }
        public List<Picture> Pictures { get; set; }

        public StaffVM()
        {
            Staff = new List<Staff>();
            Pictures = new List<Picture>();

        }
    }
    public class EditStaffVM
    {
        public Staff Staff { get; set; }
        public Picture Picture { get; set; }
    }
}