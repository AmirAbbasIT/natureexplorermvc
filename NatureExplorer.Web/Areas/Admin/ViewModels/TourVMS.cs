﻿using NatureExplorer.Database.EntitiesForFE;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static NatureExplorer.Web.Areas.Admin.ViewModels.BaseListingVM;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class TourVM
    {
        public List<Tour> tours { get; set; }
        public string searchTerm { get; set; }
        public Pager pager { get; set; }
    }
    public class ADDTourVM
    {
        public List<TourSelect> Tours { get; set; }
        public List<Facility> facilities { get; set; }
        public List<String> Hotels { get; set; }

    }
    public class EditTourVM
    {
        public Tour Tour { get; set; }
        public List<Facility> facilities { get; set; }
        public List<string> Hotels { get; internal set; }
        public List<double> BusReservationPercentages { get; set; }
        public EditTourVM()
        {
            BusReservationPercentages = new List<double>();
        }
    }

    public class TourRenew
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime DepartureDate { get; set; }
        public int Days { get; set; }
        public int Nights { get; set; }
        public string Destination { get; set; }
        public  List<TourPicture> Pictures { get; set; }
        public  List<TourDayDetail> TourDays { get; set; }
        public  List<TourNightDetail> TourNights { get; set; }
        public  List<TourPricing> TourPrices { get; set; }
        public  List<TourFacility> TourFacilities { get; set; }
        public TourRenew()
        {
            Pictures = new List<TourPicture>();
            TourDays = new List<TourDayDetail>();
            TourNights = new List<TourNightDetail>();
            TourPrices = new List<TourPricing>();
            TourFacilities = new List<TourFacility>();
        }
    }
}