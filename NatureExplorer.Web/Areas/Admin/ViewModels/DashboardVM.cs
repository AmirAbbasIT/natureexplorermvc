﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Web.Areas.Admin.ViewModels
{
    public class DashboardVM
    {
        public int Tours { get; set; }
        public int Hotels { get; set; }
        public int Users { get; set; }
        public int Bookings { get; set; }
        public int Queries { get; set; }
        public int Reviews { get; set; }
    }
}