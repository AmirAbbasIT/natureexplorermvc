﻿using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class FacilityController : Controller
    {
        // GET: Admin/Facility
        public ActionResult Index()
        {
            return View();
        }

        #region FacilitiesTable
        public ActionResult FacilityTable()
        {
            var model = FacilityService.GetFacilityServices().GetFacilities();
            return PartialView(model);
        }
        #endregion

        #region Add
        [HttpGet]
        public ActionResult AddFacility()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddFacility(Facility facility)
        {
            FacilityService.GetFacilityServices().AddFacility(facility);
            return RedirectToAction("FacilityTable");
        }
        #endregion

        #region Edit
        [HttpGet]
        public ActionResult EditFacility(int ID)
        {
            var model = FacilityService.GetFacilityServices().GetFacility(ID);
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditFacility(Facility facility)
        {
            FacilityService.GetFacilityServices().EditFacility(facility);
            return RedirectToAction("FacilityTable");
        }
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult DeleteFacility(int ID)
        {
            var result = FacilityService.GetFacilityServices().DeleteFacility(ID);
            return Redirect(Url.Action("FacilityTable", "Facility"));
        }
        #endregion
    }
}