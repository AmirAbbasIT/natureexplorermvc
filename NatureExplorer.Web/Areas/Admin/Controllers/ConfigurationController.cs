﻿using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class ConfigurationController : Controller
    {
        // GET: Admin/Configuration
        public ActionResult Index()
        {    
            return View();
        }
        public ActionResult ConfigurationTable()
        {
            var model = ConfigurationService.GetConfigurationServices().GetAllConfiguration();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult UpdateConfiguration(Configuration Config)
        {
            ConfigurationService.GetConfigurationServices().EditConfiguration(Config);
            return RedirectToAction("ConfigurationTable");
        }

        [HttpGet]
        public JsonResult TrainModel()
        {
            string path = Server.MapPath("~/RecSys_Resources/");
            ReccomedationService.GetReccomendationServices().Train_SetDefaultModel(path);
            return Json(new { Status = "Succeeded"}, JsonRequestBehavior.AllowGet);
        }
    }
}