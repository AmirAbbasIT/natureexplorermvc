﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class ValidationController : Controller
    {
        // GET: Admin/Validation
        [HttpPost]
        public string DepartureDate(DateTime DepartureDate)
        {
            if (DepartureDate > DateTime.Now)
                return "true";
            else
                return "false";
        }
    }
}