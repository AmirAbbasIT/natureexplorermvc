﻿using NatureExplorer.Database.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class ReviewController : Controller
    {
        // GET: Admin/Review
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReviewTable()
        {
            return PartialView();
        }

        public ActionResult GetUserReviews(string UserID)
        {
            var model = ReviewService.GetReviewServices().GetUserReviews(UserID);
            return PartialView(model);
        }
    }
}