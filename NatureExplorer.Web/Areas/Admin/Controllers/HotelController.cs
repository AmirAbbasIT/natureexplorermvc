﻿using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static NatureExplorer.Web.Areas.Admin.ViewModels.HotelVMS;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class HotelController : Controller
    {
        // GET: Admin/Hotel
        public ActionResult Index()
        {
            return View();
        }

        #region HotelsTable
        public ActionResult HotelTable(string search, int? pageNo, int? pageSize = 5)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new HotelVM();
            model.Hotels = HotelService.GetHotelServices().GetAllHotels(search, (int)pageNo, (int)pageSize).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(HotelService.GetHotelServices().HotelCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        #endregion

        #region Add
        [HttpGet]
        public ActionResult AddHotel()
        {
            var model = new ADDHotelVM();
            model.facilities = FacilityService.GetFacilityServices().GetFacilities();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult AddHotel(Hotel Hotel, string PicIDs)
        {
            HotelService.GetHotelServices().AddHotel(Hotel, PicIDs);
            return RedirectToAction("HotelTable");
        }
        #endregion

        #region Edit
        [HttpGet]
        public ActionResult EditHotel(int ID)
        {
            var model = new EditHotelVM();
            model.Hotel = HotelService.GetHotelServices().GetHotel(ID);
            model.Hotel.Picture = HotelService.GetHotelServices().GetHotelPictureByHotelID(model.Hotel.PictureID);
            model.Hotel.HotelFacilities = HotelService.GetHotelServices().GetHotelFacilities(model.Hotel.ID);
            model.facilities = FacilityService.GetFacilityServices().GetFacilities();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditHotel(Hotel Hotel, string PicIDs)
        {
            HotelService.GetHotelServices().EditHotel(Hotel, PicIDs);
            return RedirectToAction("HotelTable");
        }
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult DeleteHotel(int ID)
        {
            var result = HotelService.GetHotelServices().DeleteHotel(ID);
            return RedirectToAction("AuctionTable");
        }
        #endregion

 
    }
}