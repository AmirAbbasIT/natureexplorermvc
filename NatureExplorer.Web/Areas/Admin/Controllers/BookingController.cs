﻿using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static NatureExplorer.Entities.Enums;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class BookingController : Controller
    {
        // GET: Admin/Booking
        public ActionResult Index()
        {
            return View();
        }

        #region Tour BookingsTable
        public ActionResult BookingTable(string search, int? pageNo, int? pageSize = 10,int Bookingtype=1)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new TourBookingVM();
            model.bookings = BookingService.GetBookingServices().GetAllBookings(search, (int)pageNo, (int)pageSize,Bookingtype).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(BookingService.GetBookingServices().BookingCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        public ActionResult TourBookingTable(int TourID, string search, int? pageNo, int? pageSize = 10, int Bookingtype = 1)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new TourBookingVM();
            model.bookings = BookingService.GetBookingServices().GetAllTourBookings(TourID,search, (int)pageNo, (int)pageSize, Bookingtype).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(HotelService.GetHotelServices().HotelCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        #endregion
        public ActionResult BookingView(int ID)
        {
            var model = new TourBookingViewVM();
            model.Booking = BookingService.GetBookingServices().GetBooking(ID);
            model.Tour = TourService.GetTourServices().GetTour(model.Booking.RecordID);
            model.TourBus = BusService.GetBusServices().GetTourBus(model.Booking.TourBusID);
            model.Seats = BusService.GetBusServices().GetUserBusSeats(model.Booking.TourBusID, model.Booking.UserID);
            ///Getiing Payment Info
            if(model.Booking.PaymentMethodType == (int)PaymentMethod.ByCard)
            {

            }
            else if(model.Booking.PaymentMethodType == (int)PaymentMethod.ByEasyPaisa)
            {
                model.EasyPaisa = PaymentService.GetPaymentServices().GetEasyPaisaTransection(model.Booking.PaymentTransectionID);
            }
            return PartialView(model);
        }
        public bool DeleteBooking(int BookingID)
        {
            BookingService.GetBookingServices().DeleteBooking(BookingID);
            return true;
        }

        #region Hotel BookingsTable
        public ActionResult HotelBookingTable(string search, int? pageNo, int? pageSize = 6, int Bookingtype = 2)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new HotelBookingVM();
            model.bookings = BookingService.GetBookingServices().GetAllHotelBookings(search, (int)pageNo, (int)pageSize).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(BookingService.GetBookingServices().HotelBookingCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        public ActionResult HotelBookingViewTable(int HotelID, string search, int? pageNo, int? pageSize = 6)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new HotelBookingVM();
            model.bookings = BookingService.GetBookingServices().GetAllHotelBookingsByHotelID(HotelID, (int)pageNo, (int)pageSize).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(BookingService.GetBookingServices().HotelBookingCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        #endregion
        public ActionResult HotelBookingView(int ID)
        {
            var model = new HotelBookingViewVM();
            model.Booking = BookingService.GetBookingServices().GetHotelBooking(ID);
            model.Hotel = HotelService.GetHotelServices().GetHotel(model.Booking.HotelID);
            model.HotelPackage = HotelPackageService.GetHotelPackageServices().GetHotelPackage(model.Booking.HotelPackageID);
            ///Getiing Payment Info
            if (model.Booking.PaymentMethodType == (int)PaymentMethod.ByCard)
            {

            }
            else if (model.Booking.PaymentMethodType == (int)PaymentMethod.ByEasyPaisa)
            {
                model.EasyPaisa = PaymentService.GetPaymentServices().GetEasyPaisaTransection(model.Booking.PaymentTransectionID);
            }
            return PartialView(model);
        }
        public bool DeleteHotelBooking(int BookingID)
        {
            BookingService.GetBookingServices().DeleteBooking(BookingID);
            return true;
        }

        #region PaymentInfo Section
        public ActionResult PaymentInfoTable()
        {
            var PaymentInfo = BillingInfoService.GetBillingInfoServices().GetAllBillingInfo();
            return PartialView(PaymentInfo);
        }

        [HttpGet]
        public ActionResult AddInfo()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddInfo(BillingInfo Info)
        {
            BillingInfoService.GetBillingInfoServices().AddBillingInfo(Info);
            return RedirectToAction("PaymentInfoTable");
        }

       
        [HttpPost]
        public ActionResult DeleteInfo(int ID)
        {
            var result = BillingInfoService.GetBillingInfoServices().DeleteBillingInfo(ID);
            return RedirectToAction("PaymentInfoTable");
        }
        #endregion
        

    }
}