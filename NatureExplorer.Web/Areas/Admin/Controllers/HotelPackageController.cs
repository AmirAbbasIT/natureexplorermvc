﻿using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static NatureExplorer.Web.Areas.Admin.ViewModels.HotelPackageVMS;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class HotelPackageController : Controller
    {
        // GET: Admin/HotelPackage
        public ActionResult Index()
        {
            return View();
        }

        #region HotelPackagesTable
        public ActionResult HotelPackageTable(string search, int? pageNo, int? pageSize = 5)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new HotelPackageVM();
            model.HotelPackages = HotelPackageService.GetHotelPackageServices().GetAllHotelPackages(search, (int)pageNo, (int)pageSize).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(HotelPackageService.GetHotelPackageServices().HotelPackageCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        #endregion

        #region Add
        [HttpGet]
        public ActionResult AddHotelPackage()
        {
            var model = new ADDHotelPackageVM();
            model.Hotels = HotelService.GetHotelServices().GetAllHotels();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult AddHotelPackage(HotelPackage HotelPackage, string PicIDs)
        {
            HotelPackageService.GetHotelPackageServices().AddHotelPackage(HotelPackage, PicIDs);
            return RedirectToAction("HotelPackageTable", "HotelPackage");
        }
        #endregion
        #region Edit
        [HttpGet]
        public ActionResult EditHotelPackage(int ID)
        {
            var model = new EditHotelPackageVM();
            model.HotelPackage = HotelPackageService.GetHotelPackageServices().GetHotelPackage(ID);
            model.HotelPackage.Pictures = HotelPackageService.GetHotelPackageServices().GetHotelPackagePicturesByHotelPackageID(model.HotelPackage.ID);
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditHotelPackage(HotelPackage HotelPackage, string PicIDs)
        {
            HotelPackageService.GetHotelPackageServices().EditHotelPackage(HotelPackage, PicIDs);
            return RedirectToAction("HotelPackageTable", "HotelPackage");
        }
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult DeleteHotelPackage(int ID)
        {
            var result = HotelPackageService.GetHotelPackageServices().DeleteHotelPackage(ID);
            return RedirectToAction("HotelPackageTable", "HotelPackage");
        }
        #endregion

       
    }
}
