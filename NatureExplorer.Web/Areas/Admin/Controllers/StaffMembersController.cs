﻿using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class StaffMembersController : Controller
    {
        // GET: Admin/StaffMembers
        public ActionResult Index()
        {
            return View();
        }

        #region StaffsTable
        public ActionResult StaffTable()
        {
           var model = StaffMemberService.GetStaffServices().GetAllStaff().ToList();
            return PartialView(model);
        }
        #endregion

        #region Add
        [HttpGet]
        public ActionResult AddStaff()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddStaff(Staff Staff, string PicIDs)
        {
            StaffMemberService.GetStaffServices().AddStaff(Staff, PicIDs);
            return RedirectToAction("StaffTable");
        }
        #endregion

        #region Edit
        [HttpGet]
        public ActionResult EditStaff(int ID)
        {
            var model = new EditStaffVM();
            model.Staff = StaffMemberService.GetStaffServices().GetStaff(ID);
            model.Picture = StaffMemberService.GetStaffServices().GetStaffPictureByStaffID(model.Staff.PictureID);
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditStaff(Staff Staff, string PicIDs)
        {
            StaffMemberService.GetStaffServices().EditStaff(Staff, PicIDs);
            return RedirectToAction("StaffTable");
        }
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult DeleteStaff(int ID)
        {
            var result = StaffMemberService.GetStaffServices().DeleteStaff(ID);
            return RedirectToAction("StaffTable");
        }
        #endregion
    }
}