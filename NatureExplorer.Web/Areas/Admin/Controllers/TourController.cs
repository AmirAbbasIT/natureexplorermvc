﻿using NatureExplorer.Database.Services;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class TourController : Controller
    {
        // GET: Admin/Tour
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult AuctionDetail(int ID)
        //{
        //    var model = new detailAuctionVM();
        //    model.Auction = AuctionServices.GetAuctionServices().GetAuction(ID);
        //    model.EntityType = (int)EntityType.Auction;
        //    model.Comments = CommentServices.GetCommentServices().GetComments((int)EntityType.Auction, ID);
        //    model.HighestBidder = BidServices.GetBidServices().getHighestBidder(ID);
        //    model.PictureUrls = AuctionServices.GetAuctionServices().GetAuctionPictures(ID);
        //    return View(model);
        //}

        #region ToursTable
        public ActionResult TourTable(string search, int? pageNo, int? pageSize = 5)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new TourVM();
            model.tours = TourService.GetTourServices().GetAllTours(search, (int)pageNo, (int)pageSize).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(TourService.GetTourServices().TourCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        #endregion

        #region Add
        [HttpGet]
        public ActionResult AddTour()
        {
            var model = new ADDTourVM();
            model.facilities = FacilityService.GetFacilityServices().GetFacilities();
            model.Hotels = HotelService.GetHotelServices().GetHotelNames();
            model.Tours = TourService.GetTourServices().GetToursForSelectList();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult AddTour(Tour tour, string PicIDs)
        {
            string path = Server.MapPath("~/RecSys_Resources/");
            TourService.GetTourServices().AddTour(tour, PicIDs,path);
            var subscribers = SubscribersService.GetSubscriberServices().GetAllSubscribers();
           
            foreach(var sub in subscribers)
            {
                var message = "Hello! " + sub.Name + " ,A new Journey to " + tour.Destination + " is waiting for U Visit Our Website to be a part of Amazing Tour";
                NotificationService.GetNotificationServices().SendEmail(sub.Mail, "Nature Explorer", message);
                var number = sub.Contact.Remove(0,1);
                NotificationService.GetNotificationServices().SendSMS(number, message);
            }
            return RedirectToAction("TourTable");
        }
        #endregion

        //#region PostComment
        //[HttpPost]
        //public JsonResult PostComment(Comment comment)
        //{
        //    comment.TimeStamp = DateTime.Now;
        //    comment.UserID = User.Identity.GetUserId();
        //    comment.Ratings = comment.Ratings;
        //    CommentServices.GetCommentServices().AddComment(comment);
        //    JsonResult result = new JsonResult();
        //    result.Data = new { Success = true };
        //    return result;
        //}
        //#endregion

        #region Edit
        [HttpGet]
        public ActionResult EditTour(int ID)
        {
            var model = new EditTourVM();
            model.Tour=TourService.GetTourServices().GetTour(ID);
            model.Tour.Pictures = TourService.GetTourServices().GetTourPicturesByTourID(model.Tour.ID);
            model.Tour.TourDays = TourService.GetTourServices().GetToursDayDetail(model.Tour.ID);
            model.Tour.TourPrices = TourService.GetTourServices().GetToursPricingDetail(model.Tour.ID);
            model.Tour.TourFacilities = TourService.GetTourServices().GetTourFacilities(model.Tour.ID);
            model.Tour.TourNights = TourService.GetTourServices().GetTourNightDetail(model.Tour.ID);
            model.Tour.TourBuses = BusService.GetBusServices().GetTourBuses(model.Tour.ID);
            model.facilities = FacilityService.GetFacilityServices().GetFacilities();
            model.Hotels = HotelService.GetHotelServices().GetHotelNames();
            for(int i=0;i<model.Tour.TourBuses.Count;i++)
            {
                model.BusReservationPercentages.Add(BusService.GetBusServices().GetPercentage(model.Tour.TourBuses[i].ID));
            }
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditTour(Tour tour,string PicIDs)
        {
            TourService.GetTourServices().EditTour(tour,PicIDs);
            return RedirectToAction("TourTable");
        }
        #endregion

        #region RenewTour
        [HttpPost]
        public JsonResult RenewTour(int ID)
        {
            var Tour = TourService.GetTourServices().GetTour(ID);
            Tour.Pictures = TourService.GetTourServices().GetTourPicturesByTourID(ID);
            Tour.TourDays = TourService.GetTourServices().GetToursDayDetail(ID);
            Tour.TourPrices = TourService.GetTourServices().GetToursPricingDetail(ID);
            Tour.TourFacilities = TourService.GetTourServices().GetTourFacilities(ID);
            Tour.TourNights = TourService.GetTourServices().GetTourNightDetail(ID);

            //Parse Values To TourRenew//

            var Renew = new TourRenew()
            {
                Title = Tour.Title,
                Days = Tour.Days,
                DepartureDate = DateTime.Now,
                Nights=Tour.Nights,
                Destination=Tour.Destination,
                Pictures=Tour.Pictures,
                TourDays=Tour.TourDays,
                TourNights=Tour.TourNights,
                TourFacilities=Tour.TourFacilities,
                TourPrices=Tour.TourPrices

            };
            var result = new JsonResult();
            result.ContentType = "application/json";
            result.Data = Renew;
            return result;
        }
        #endregion
        #region Delete
        [HttpGet]
        public ActionResult DeleteTour(int ID)
        {
            var result = TourService.GetTourServices().DeleteTour(ID);
            return RedirectToAction("TourTable");
        }
        #endregion

       
    }
}