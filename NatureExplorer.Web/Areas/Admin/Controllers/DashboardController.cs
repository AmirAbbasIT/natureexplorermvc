﻿using NatureExplorer.Database.Services;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            var model = new DashboardVM();
            model.Tours = TourService.GetTourServices().TourCount(null);
            model.Hotels = HotelService.GetHotelServices().HotelCount(null);
            model.Bookings = BookingService.GetBookingServices().BookingCount(null);
            model.Reviews = ReviewService.GetReviewServices().ReviewsCount();
            model.Queries = Querieservice.GetQuerieservices().QueryCount(null);
            model.Users = UserService.GetUserServices().UsersCount();
            return View(model);
        }
    }
}