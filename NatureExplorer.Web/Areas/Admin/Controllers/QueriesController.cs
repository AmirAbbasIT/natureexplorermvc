﻿using NatureExplorer.Database.Services;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class QueriesController : Controller
    {
        // GET: Admin/Queries
        public ActionResult Index()
        {
            return View();
        }

        #region QueriesTable
        public ActionResult QueryTable(string search, int? pageNo, int? pageSize = 5)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            var model = new QueryVM();
            model.queries = Querieservice.GetQuerieservices().GetAllQueries(search, (int)pageNo, (int)pageSize).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(Querieservice.GetQuerieservices().QueryCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        #endregion

        [HttpGet]
        public ActionResult QueryMessage(int ID)
        {
            var Query = Querieservice.GetQuerieservices().GetQuery(ID);
            return PartialView(Query);
        }
        [HttpPost]
        public async Task ReplyMessage(int ID,string reply)
        {
            var Query = Querieservice.GetQuerieservices().GetQuery(ID);
            await NotificationService.GetNotificationServices().SendEmail(Query.Email,"Reply for Query",reply);
        }
        #region Delete
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            var result = Querieservice.GetQuerieservices().DeleteQuery(ID);
            return RedirectToAction("QueryTable");
        }
        #endregion
    }
}