﻿using Microsoft.AspNet.Identity.Owin;
using NatureExplorer.Database.Managers;
using NatureExplorer.Entities;
using NatureExplorer.Web.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NatureExplorer.Web.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        // GET: Admin/Account
        NEUserManager Usermanager;
        NERoleManager RoleManager;
        public ActionResult Index()
        {
            return View();
        }

        #region Handling Users....  
        //partial view controller for users table
        public ActionResult UsersTable(string search, int? pageNo = 1, int? pageSize = 5)
        {
            int PageNo = Convert.ToInt32(pageNo != null ? pageNo : 1);
            Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
            RoleManager = HttpContext.GetOwinContext().Get<NERoleManager>();
            //////Now do your work here...........
            var users = new List<NEUser>();
            users = Usermanager.Users.ToList();
            if (!string.IsNullOrEmpty(search))
            {
                users = users.Where(u => u.Email.ToLower().Contains(search.ToLower()))
                         .Skip((PageNo - 1) * (int)pageSize)
                         .Take((int)pageSize)
                         .ToList();
            }
            else
            {
                users = users
                    .Skip((PageNo - 1) * (int)pageSize)
                    .Take((int)pageSize)
                    .ToList();
            }
            var model = new UsersVM();
            model.users = users;
            model.roles = RoleManager.Roles.ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(users.Count, pageNo, (int)pageSize);
            return PartialView(model);
        }
        public async Task<ActionResult> UserDetail(string ID)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("UserDetailForm");
            }
            ///
            Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
            RoleManager = HttpContext.GetOwinContext().Get<NERoleManager>();
            //Im Getting Id with braces so first to remove braces
            if (ID.Length > 0 && ID[0] == '{')
                ID = ID.Substring(1, ID.Length - 2);
            ///
            var model = new UserDetailVM();
            var user = await Usermanager.FindByIdAsync(ID);
            model.User = user;
            return View(model);
        }
        //Updating User Details in Detail partial view
        [HttpPost]
        public async Task<ActionResult> UserDetail(NEUser User)
        {
            
                Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
                var OrigionalUser = await Usermanager.FindByIdAsync(User.Id);
                OrigionalUser.Adress = User.Adress;
                OrigionalUser.FullName = User.FullName;
                OrigionalUser.ContantNo = User.ContantNo;
                OrigionalUser.PhoneNumber = User.ContantNo;
                await Usermanager.UpdateAsync(OrigionalUser);
                return RedirectToAction("UserDetail", new { ID = User.Id });
            
            
        }
        ///User Role Section...... Where user can see and add or delete roles through Ajax....
        public async Task<ActionResult> UserRoles(String UserID)
        {
            Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
            RoleManager = HttpContext.GetOwinContext().Get<NERoleManager>();
            var model = new UserRolesDetailVM();
            model.User = await Usermanager.FindByIdAsync(UserID);
            model.AvailableRoles = RoleManager.Roles.ToList();
            model.UserRoles = model.User.Roles.Select(userRole => model.AvailableRoles.FirstOrDefault(r => r.Id == userRole.RoleId)).ToList();
            model.AvailableRoles = model.AvailableRoles.Where(r => model.UserRoles.FirstOrDefault(role => role.Id == r.Id) == null).ToList();
            return PartialView(model);
        }
        public async Task<ActionResult> AddRole(string userID, string role)
        {
            Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
            RoleManager = HttpContext.GetOwinContext().Get<NERoleManager>();

            var user = await Usermanager.FindByIdAsync(userID);
            if (user != null)
            {
                var result = await Usermanager.AddToRoleAsync(user.Id, role);
                if (result.Succeeded)
                {
                    return RedirectToAction("UserRoles", "Account", new { UserID = userID });
                }
            }
            return RedirectToAction("UserRoles", "Account", new { UserID = userID });
        }
        public async Task<ActionResult> DeleteRole(string userID, string role)
        {
            Usermanager = HttpContext.GetOwinContext().GetUserManager<NEUserManager>();
            RoleManager = HttpContext.GetOwinContext().Get<NERoleManager>();

            var user = await Usermanager.FindByIdAsync(userID);
            if (user != null)
            {
                var result = await Usermanager.RemoveFromRoleAsync(user.Id, role);
                if (result.Succeeded)
                {
                    return RedirectToAction("UserRoles", "Account", new { UserID = userID });
                }
            }
            return RedirectToAction("UserRoles", "Account", new { UserID = userID });
        }
        //////handling User Comments ....
        //public ActionResult UserComments(String UserID)
        //{
        //    var model = new UserDetail_CommentsVM();
        //    model.UserComments = CommentServices.GetCommentServices().GetUserComments(UserID, (int)EntityType.Auction);
        //    return PartialView(model);
        //}
        #endregion
    }
}