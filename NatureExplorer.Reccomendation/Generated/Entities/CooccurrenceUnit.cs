﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Reccomendation.Generated.Entities
{
    
        /// <summary>
        /// Defines values for CooccurrenceUnit.
        /// </summary>
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum CooccurrenceUnit
        {
            [System.Runtime.Serialization.EnumMember(Value = "User")]
            User,
            [System.Runtime.Serialization.EnumMember(Value = "Timestamp")]
            Timestamp
        }
    
}
