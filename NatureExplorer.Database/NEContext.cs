﻿using Microsoft.AspNet.Identity.EntityFramework;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database
{
    public class NEContext : IdentityDbContext<NEUser>
    {
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<TourDayDetail> TourDayDetails { get; set; }
        public DbSet<TourNightDetail> TourNightDetails { get; set; }
        public DbSet<TourPricing> TourPricing { get; set; }
        public DbSet<TourPicture> TourPictures { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<TourFacility> TourFacilities { get; set; }
        public DbSet<HotelFacility> HotelFacilities { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<HotelPackage> HotelPackages { get; set; }
        public DbSet<HotelPackagePicture> HotelPackagePictures { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Query> Queries { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<RoomReservation> RoomReservations { get; set; }
        public DbSet<EasyPaisa> EasyPisaPayments { get; set; }
        public DbSet<BillingInfo> BillingInfo { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }

        public DbSet<TourBus> TourBuses { get; set; }
        public DbSet<BusSeat> TourBusSeats { get; set; }

        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<Staff> Staff { get; set; }
        public DbSet<Subscribe> Subscribers { get; set; }
        public NEContext():base("NatureExplorerDatabase")
        {

            System.Data.Entity.Database.SetInitializer<NEContext>(new NEDbInitializer());
            if (!Database.Exists())
            {
                Database.Initialize(true);
            }
        }

        //For Accessing instance in Startup.Auth
        public static NEContext Create()
        {
            return new NEContext();
        }
    }
}
