namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatingConfigurationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Configurations", "FaceBook", c => c.String());
            AddColumn("dbo.Configurations", "Instagram", c => c.String());
            AddColumn("dbo.Configurations", "Twitter", c => c.String());
            AddColumn("dbo.Configurations", "Contact", c => c.String());
            Sql("Insert Into Configurations(Email,Port,CompanyEmail,CompanyPhone,FacebookAppID,FacebookSecretID,GoogleAppID,GoogleSecretID,AuthorizeNetTransectionID,AuthorizeNetAppID) Values('',0,'','','','','','','','')");       
                }
        
        public override void Down()
        {
            DropColumn("dbo.Configurations", "Contact");
            DropColumn("dbo.Configurations", "Twitter");
            DropColumn("dbo.Configurations", "Instagram");
            DropColumn("dbo.Configurations", "FaceBook");
        }
    }
}
