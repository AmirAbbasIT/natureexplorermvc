namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingFieldToQueryTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Queries", "MobileNumber", c => c.String());
            AddColumn("dbo.Queries", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Queries", "Date");
            DropColumn("dbo.Queries", "MobileNumber");
        }
    }
}
