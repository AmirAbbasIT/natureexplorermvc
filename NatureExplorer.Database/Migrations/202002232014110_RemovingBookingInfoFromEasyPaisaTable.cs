namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovingBookingInfoFromEasyPaisaTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EasyPaisas", "bookingID", "dbo.Bookings");
            DropIndex("dbo.EasyPaisas", new[] { "bookingID" });
            DropColumn("dbo.EasyPaisas", "bookingID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EasyPaisas", "bookingID", c => c.Int(nullable: false));
            CreateIndex("dbo.EasyPaisas", "bookingID");
            AddForeignKey("dbo.EasyPaisas", "bookingID", "dbo.Bookings", "ID", cascadeDelete: true);
        }
    }
}
