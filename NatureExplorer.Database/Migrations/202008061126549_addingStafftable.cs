namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingStafftable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Designation = c.String(),
                        Address = c.String(),
                        Contact = c.String(),
                        Email = c.String(),
                        PictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .Index(t => t.PictureID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Staffs", "PictureID", "dbo.Pictures");
            DropIndex("dbo.Staffs", new[] { "PictureID" });
            DropTable("dbo.Staffs");
        }
    }
}
