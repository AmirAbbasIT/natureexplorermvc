namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Issue : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HotelPackages", "HotelID", "dbo.Hotels");
            DropIndex("dbo.HotelPackages", new[] { "HotelID" });
            AlterColumn("dbo.HotelPackages", "HotelID", c => c.Int());
            CreateIndex("dbo.HotelPackages", "HotelID");
            AddForeignKey("dbo.HotelPackages", "HotelID", "dbo.Hotels", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HotelPackages", "HotelID", "dbo.Hotels");
            DropIndex("dbo.HotelPackages", new[] { "HotelID" });
            AlterColumn("dbo.HotelPackages", "HotelID", c => c.Int(nullable: false));
            CreateIndex("dbo.HotelPackages", "HotelID");
            AddForeignKey("dbo.HotelPackages", "HotelID", "dbo.Hotels", "ID", cascadeDelete: true);
        }
    }
}
