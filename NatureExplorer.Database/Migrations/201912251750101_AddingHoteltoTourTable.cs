namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingHoteltoTourTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tours", "HotelID", c => c.Int());
            CreateIndex("dbo.Tours", "HotelID");
            AddForeignKey("dbo.Tours", "HotelID", "dbo.Hotels", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tours", "HotelID", "dbo.Hotels");
            DropIndex("dbo.Tours", new[] { "HotelID" });
            DropColumn("dbo.Tours", "HotelID");
        }
    }
}
