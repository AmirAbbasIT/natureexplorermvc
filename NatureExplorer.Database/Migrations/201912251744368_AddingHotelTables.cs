namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingHotelTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Facilities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.HotelFacilities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HotelID = c.Int(nullable: false),
                        FacilityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Facilities", t => t.FacilityID, cascadeDelete: true)
                .Index(t => t.FacilityID);
            
            CreateTable(
                "dbo.HotelPackagePictures",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HotelPackageID = c.Int(nullable: false),
                        PictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.HotelPackages", t => t.HotelPackageID, cascadeDelete: true)
                .Index(t => t.HotelPackageID)
                .Index(t => t.PictureID);
            
            CreateTable(
                "dbo.HotelPackages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HotelID = c.Int(nullable: false),
                        Name = c.String(),
                        Desciption = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Hotels", t => t.HotelID, cascadeDelete: true)
                .Index(t => t.HotelID);
            
            CreateTable(
                "dbo.HotelPictures",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HotelID = c.Int(nullable: false),
                        PictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .Index(t => t.PictureID);
            
            CreateTable(
                "dbo.Hotels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Desciption = c.String(),
                        Address = c.String(),
                        Contact = c.String(),
                        Email = c.String(),
                        Picture_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HotelPictures", t => t.Picture_ID)
                .Index(t => t.Picture_ID);
            
            CreateTable(
                "dbo.TourFacilities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TourID = c.Int(nullable: false),
                        FacilityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Facilities", t => t.FacilityID, cascadeDelete: true)
                .ForeignKey("dbo.Tours", t => t.TourID, cascadeDelete: true)
                .Index(t => t.TourID)
                .Index(t => t.FacilityID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TourFacilities", "TourID", "dbo.Tours");
            DropForeignKey("dbo.TourFacilities", "FacilityID", "dbo.Facilities");
            DropForeignKey("dbo.Hotels", "Picture_ID", "dbo.HotelPictures");
            DropForeignKey("dbo.HotelPackages", "HotelID", "dbo.Hotels");
            DropForeignKey("dbo.HotelPictures", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.HotelPackagePictures", "HotelPackageID", "dbo.HotelPackages");
            DropForeignKey("dbo.HotelPackagePictures", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.HotelFacilities", "FacilityID", "dbo.Facilities");
            DropIndex("dbo.TourFacilities", new[] { "FacilityID" });
            DropIndex("dbo.TourFacilities", new[] { "TourID" });
            DropIndex("dbo.Hotels", new[] { "Picture_ID" });
            DropIndex("dbo.HotelPictures", new[] { "PictureID" });
            DropIndex("dbo.HotelPackages", new[] { "HotelID" });
            DropIndex("dbo.HotelPackagePictures", new[] { "PictureID" });
            DropIndex("dbo.HotelPackagePictures", new[] { "HotelPackageID" });
            DropIndex("dbo.HotelFacilities", new[] { "FacilityID" });
            DropTable("dbo.TourFacilities");
            DropTable("dbo.Hotels");
            DropTable("dbo.HotelPictures");
            DropTable("dbo.HotelPackages");
            DropTable("dbo.HotelPackagePictures");
            DropTable("dbo.HotelFacilities");
            DropTable("dbo.Facilities");
        }
    }
}
