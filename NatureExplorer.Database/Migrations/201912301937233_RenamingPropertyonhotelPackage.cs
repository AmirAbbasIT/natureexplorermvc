namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamingPropertyonhotelPackage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HotelPackages", "Description", c => c.String());
            DropColumn("dbo.HotelPackages", "Desciption");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HotelPackages", "Desciption", c => c.String());
            DropColumn("dbo.HotelPackages", "Description");
        }
    }
}
