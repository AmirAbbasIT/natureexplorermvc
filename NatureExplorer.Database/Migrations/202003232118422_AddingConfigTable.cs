namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingConfigTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Configurations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Port = c.Int(nullable: false),
                        CompanyEmail = c.String(),
                        CompanyPhone = c.String(),
                        FacebookAppID = c.String(),
                        FacebookSecretID = c.String(),
                        GoogleAppID = c.String(),
                        GoogleSecretID = c.String(),
                        AuthorizeNetTransectionID = c.String(),
                        AuthorizeNetAppID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Configurations");
        }
    }
}
