// <auto-generated />
namespace NatureExplorer.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.2-31219")]
    public sealed partial class AddingPictureFK1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddingPictureFK1));
        
        string IMigrationMetadata.Id
        {
            get { return "202008080830324_AddingPictureFK1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
