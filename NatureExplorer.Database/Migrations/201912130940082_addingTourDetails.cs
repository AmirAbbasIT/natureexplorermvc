namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingTourDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TourDayDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TourID = c.Int(nullable: false),
                        DayNumber = c.Int(nullable: false),
                        Description = c.String(),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tours", t => t.TourID, cascadeDelete: true)
                .Index(t => t.TourID);
            
            CreateTable(
                "dbo.TourPricings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TourID = c.Int(nullable: false),
                        Location = c.String(),
                        SinglePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CouplePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tours", t => t.TourID, cascadeDelete: true)
                .Index(t => t.TourID);
            
            AddColumn("dbo.Tours", "Destination", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TourPricings", "TourID", "dbo.Tours");
            DropForeignKey("dbo.TourDayDetails", "TourID", "dbo.Tours");
            DropIndex("dbo.TourPricings", new[] { "TourID" });
            DropIndex("dbo.TourDayDetails", new[] { "TourID" });
            DropColumn("dbo.Tours", "Destination");
            DropTable("dbo.TourPricings");
            DropTable("dbo.TourDayDetails");
        }
    }
}
