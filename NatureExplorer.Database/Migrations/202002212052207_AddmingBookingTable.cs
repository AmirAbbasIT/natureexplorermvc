namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddmingBookingTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.String(maxLength: 128),
                        EntityType = c.Int(nullable: false),
                        RecordID = c.Int(nullable: false),
                        Persons = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        Location = c.String(),
                        PaymentMethodType = c.Int(nullable: false),
                        PaymentTransectionID = c.Int(nullable: false),
                        Processed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bookings", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Bookings", new[] { "UserID" });
            DropTable("dbo.Bookings");
        }
    }
}
