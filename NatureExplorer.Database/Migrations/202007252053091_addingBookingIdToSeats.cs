namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingBookingIdToSeats : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusSeats", "BookingID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusSeats", "BookingID");
        }
    }
}
