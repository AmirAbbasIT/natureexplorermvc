namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingRoomReservationField : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.RoomReservations", new[] { "User_Id" });
            DropColumn("dbo.RoomReservations", "UserID");
            RenameColumn(table: "dbo.RoomReservations", name: "User_Id", newName: "UserID");
            AddColumn("dbo.RoomReservations", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RoomReservations", "UserID", c => c.String(maxLength: 128));
            CreateIndex("dbo.RoomReservations", "UserID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.RoomReservations", new[] { "UserID" });
            AlterColumn("dbo.RoomReservations", "UserID", c => c.Int(nullable: false));
            DropColumn("dbo.RoomReservations", "Date");
            RenameColumn(table: "dbo.RoomReservations", name: "UserID", newName: "User_Id");
            AddColumn("dbo.RoomReservations", "UserID", c => c.Int(nullable: false));
            CreateIndex("dbo.RoomReservations", "User_Id");
        }
    }
}
