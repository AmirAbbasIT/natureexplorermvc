namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatingDb2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RoomReservations", "HotelID", "dbo.Hotels");
            DropForeignKey("dbo.RoomReservations", "HotelPackageID", "dbo.HotelPackages");
            DropIndex("dbo.RoomReservations", new[] { "HotelID" });
            DropIndex("dbo.RoomReservations", new[] { "HotelPackageID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.RoomReservations", "HotelPackageID");
            CreateIndex("dbo.RoomReservations", "HotelID");
            AddForeignKey("dbo.RoomReservations", "HotelPackageID", "dbo.HotelPackages", "ID", cascadeDelete: true);
            AddForeignKey("dbo.RoomReservations", "HotelID", "dbo.Hotels", "ID", cascadeDelete: true);
        }
    }
}
