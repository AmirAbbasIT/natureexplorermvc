namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatingDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TourPictures",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TourID = c.Int(nullable: false),
                        PictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.Tours", t => t.TourID, cascadeDelete: true)
                .Index(t => t.TourID)
                .Index(t => t.PictureID);
            
            CreateTable(
                "dbo.Tours",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        DepartureDate = c.DateTime(nullable: false),
                        Days = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TourPictures", "TourID", "dbo.Tours");
            DropForeignKey("dbo.TourPictures", "PictureID", "dbo.Pictures");
            DropIndex("dbo.TourPictures", new[] { "PictureID" });
            DropIndex("dbo.TourPictures", new[] { "TourID" });
            DropTable("dbo.Tours");
            DropTable("dbo.TourPictures");
        }
    }
}
