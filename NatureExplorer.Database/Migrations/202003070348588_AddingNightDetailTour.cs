namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingNightDetailTour : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TourNightDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NightNo = c.Int(nullable: false),
                        HotelToStay = c.String(),
                        TourID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tours", t => t.TourID, cascadeDelete: true)
                .Index(t => t.TourID);
            
            AddColumn("dbo.Tours", "Nights", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TourNightDetails", "TourID", "dbo.Tours");
            DropIndex("dbo.TourNightDetails", new[] { "TourID" });
            DropColumn("dbo.Tours", "Nights");
            DropTable("dbo.TourNightDetails");
        }
    }
}
