namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatingNotificationTableFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "DateAdded", c => c.DateTime(nullable: false));
            AddColumn("dbo.Notifications", "action", c => c.String());
            AddColumn("dbo.Notifications", "controller", c => c.String());
            AddColumn("dbo.Notifications", "resourceID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "resourceID");
            DropColumn("dbo.Notifications", "controller");
            DropColumn("dbo.Notifications", "action");
            DropColumn("dbo.Notifications", "DateAdded");
        }
    }
}
