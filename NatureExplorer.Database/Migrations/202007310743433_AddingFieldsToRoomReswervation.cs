namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingFieldsToRoomReswervation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RoomReservations", "HotelID", c => c.Int(nullable: false));
            CreateIndex("dbo.RoomReservations", "HotelID");
            AddForeignKey("dbo.RoomReservations", "HotelID", "dbo.Hotels", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoomReservations", "HotelID", "dbo.Hotels");
            DropIndex("dbo.RoomReservations", new[] { "HotelID" });
            DropColumn("dbo.RoomReservations", "HotelID");
        }
    }
}
