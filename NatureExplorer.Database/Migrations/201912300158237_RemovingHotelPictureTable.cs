namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovingHotelPictureTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HotelPictures", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.Hotels", "Picture_ID", "dbo.HotelPictures");
            DropIndex("dbo.HotelPictures", new[] { "PictureID" });
            DropIndex("dbo.Hotels", new[] { "Picture_ID" });
            RenameColumn(table: "dbo.Hotels", name: "Picture_ID", newName: "PictureID");
            AlterColumn("dbo.Hotels", "PictureID", c => c.Int(nullable: false));
            CreateIndex("dbo.Hotels", "PictureID");
            AddForeignKey("dbo.Hotels", "PictureID", "dbo.Pictures", "ID", cascadeDelete: false);
            DropTable("dbo.HotelPictures");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.HotelPictures",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HotelID = c.Int(nullable: false),
                        PictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.Hotels", "PictureID", "dbo.Pictures");
            DropIndex("dbo.Hotels", new[] { "PictureID" });
            AlterColumn("dbo.Hotels", "PictureID", c => c.Int());
            RenameColumn(table: "dbo.Hotels", name: "PictureID", newName: "Picture_ID");
            CreateIndex("dbo.Hotels", "Picture_ID");
            CreateIndex("dbo.HotelPictures", "PictureID");
            AddForeignKey("dbo.Hotels", "Picture_ID", "dbo.HotelPictures", "ID");
            AddForeignKey("dbo.HotelPictures", "PictureID", "dbo.Pictures", "ID", cascadeDelete: true);
        }
    }
}
