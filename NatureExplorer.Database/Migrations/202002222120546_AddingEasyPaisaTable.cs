namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingEasyPaisaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EasyPaisas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TransectionID = c.String(),
                        Secret = c.String(),
                        RecievingCNIC = c.String(),
                        bookingID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Bookings", t => t.bookingID, cascadeDelete: true)
                .Index(t => t.bookingID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EasyPaisas", "bookingID", "dbo.Bookings");
            DropIndex("dbo.EasyPaisas", new[] { "bookingID" });
            DropTable("dbo.EasyPaisas");
        }
    }
}
