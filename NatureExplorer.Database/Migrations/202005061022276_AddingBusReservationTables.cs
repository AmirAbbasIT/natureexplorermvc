namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingBusReservationTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TourBus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TourID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tours", t => t.TourID, cascadeDelete: true)
                .Index(t => t.TourID);
            
            CreateTable(
                "dbo.BusSeats",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SeatNo = c.Int(nullable: false),
                        Status = c.String(),
                        TourBusID = c.Int(nullable: false),
                        UserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TourBus", t => t.TourBusID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.TourBusID)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TourBus", "TourID", "dbo.Tours");
            DropForeignKey("dbo.BusSeats", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.BusSeats", "TourBusID", "dbo.TourBus");
            DropIndex("dbo.BusSeats", new[] { "UserID" });
            DropIndex("dbo.BusSeats", new[] { "TourBusID" });
            DropIndex("dbo.TourBus", new[] { "TourID" });
            DropTable("dbo.BusSeats");
            DropTable("dbo.TourBus");
        }
    }
}
