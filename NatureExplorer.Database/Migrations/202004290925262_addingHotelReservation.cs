namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingHotelReservation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RoomReservations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Persons = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserID = c.Int(nullable: false),
                        CheckIn = c.DateTime(nullable: false),
                        CheckOut = c.DateTime(nullable: false),
                        HotelPackageID = c.Int(nullable: false),
                        PaymentMethodType = c.Int(nullable: false),
                        PaymentTransectionID = c.Int(nullable: false),
                        Processed = c.Boolean(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HotelPackages", t => t.HotelPackageID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.HotelPackageID)
                .Index(t => t.User_Id);
            
            AddColumn("dbo.HotelPackages", "Capacity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoomReservations", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.RoomReservations", "HotelPackageID", "dbo.HotelPackages");
            DropIndex("dbo.RoomReservations", new[] { "User_Id" });
            DropIndex("dbo.RoomReservations", new[] { "HotelPackageID" });
            DropColumn("dbo.HotelPackages", "Capacity");
            DropTable("dbo.RoomReservations");
        }
    }
}
