namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovingHotelFromTour : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tours", "HotelID", "dbo.Hotels");
            DropIndex("dbo.Tours", new[] { "HotelID" });
            DropColumn("dbo.Tours", "HotelID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tours", "HotelID", c => c.Int());
            CreateIndex("dbo.Tours", "HotelID");
            AddForeignKey("dbo.Tours", "HotelID", "dbo.Hotels", "ID");
        }
    }
}
