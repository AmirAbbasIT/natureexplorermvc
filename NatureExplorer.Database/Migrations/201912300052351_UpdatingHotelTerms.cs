namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatingHotelTerms : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.HotelFacilities", "HotelID");
            AddForeignKey("dbo.HotelFacilities", "HotelID", "dbo.Hotels", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HotelFacilities", "HotelID", "dbo.Hotels");
            DropIndex("dbo.HotelFacilities", new[] { "HotelID" });
        }
    }
}
