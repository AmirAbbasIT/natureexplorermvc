namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovingPortAddingPasswordInConfTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Configurations", "Password", c => c.String());
            DropColumn("dbo.Configurations", "Port");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Configurations", "Port", c => c.Int(nullable: false));
            DropColumn("dbo.Configurations", "Password");
        }
    }
}
