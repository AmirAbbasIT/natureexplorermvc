namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingBusIDFieldToBooking : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bookings", "TourBusID", c => c.Int(nullable: false));
            AddColumn("dbo.TourBus", "Booking_ID", c => c.Int());
            CreateIndex("dbo.TourBus", "Booking_ID");
            AddForeignKey("dbo.TourBus", "Booking_ID", "dbo.Bookings", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TourBus", "Booking_ID", "dbo.Bookings");
            DropIndex("dbo.TourBus", new[] { "Booking_ID" });
            DropColumn("dbo.TourBus", "Booking_ID");
            DropColumn("dbo.Bookings", "TourBusID");
        }
    }
}
