namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamingDecriptionPropertyofHotel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hotels", "Description", c => c.String());
            DropColumn("dbo.Hotels", "Desciption");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Hotels", "Desciption", c => c.String());
            DropColumn("dbo.Hotels", "Description");
        }
    }
}
