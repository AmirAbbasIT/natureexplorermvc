namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingReccomendationsystemSettinginConfiguration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Configurations", "ReccomendationSystem", c => c.Boolean(nullable: false));
            AddColumn("dbo.Configurations", "AppUrl", c => c.String());
            AddColumn("dbo.Configurations", "AdminKey", c => c.String());
            AddColumn("dbo.Configurations", "ReccomendationKey", c => c.String());
            AddColumn("dbo.Configurations", "StorageConnectionString", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Configurations", "StorageConnectionString");
            DropColumn("dbo.Configurations", "ReccomendationKey");
            DropColumn("dbo.Configurations", "AdminKey");
            DropColumn("dbo.Configurations", "AppUrl");
            DropColumn("dbo.Configurations", "ReccomendationSystem");
        }
    }
}
