namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingJazzCashEasyPaisaInfoTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillingInfoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Info = c.String(),
                        IsJazzCash = c.Boolean(nullable: false),
                        IsEasyPaisa = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BillingInfoes");
        }
    }
}
