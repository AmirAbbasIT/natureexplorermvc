namespace NatureExplorer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingPictureFK1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Staffs", "PictureID", "dbo.Pictures");
            DropIndex("dbo.Staffs", new[] { "PictureID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Staffs", "PictureID");
            AddForeignKey("dbo.Staffs", "PictureID", "dbo.Pictures", "ID", cascadeDelete: true);
        }
    }
}
