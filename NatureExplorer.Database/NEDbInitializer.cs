﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NatureExplorer.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace NatureExplorer.Database
{
     public class NEDbInitializer :CreateDatabaseIfNotExists<NEContext>
    {
        protected override void Seed(NEContext context)
        {
              GenerateRoles(context);
              GenerateUser(context);
        }

        #region Generate InitialUsers & Roles for Admin
        public void GenerateUser(NEContext context)
        {
            var userStore = new UserStore<NEUser>(context);
            var userManager = new UserManager<NEUser>(userStore);
            var User = new NEUser()
            {
                Email = "admin@mail.com",
                UserName = "Admin"
            };
            var password = "admin123";
            if (userManager.FindByEmail(User.Email) == null)
            {
                var result = userManager.Create(User, password);
                if (result.Succeeded)
                {
                    userManager.AddToRole(User.Id, "Administrator");
                    userManager.AddToRole(User.Id, "User");
                }
            }
        }
        public void GenerateRoles(NEContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            //Creating Default Roles.....
            var listofRoles = new List<IdentityRole>();
            listofRoles.Add(new IdentityRole() { Name = "Administrator" });
            listofRoles.Add(new IdentityRole() { Name = "User" });
            foreach (IdentityRole role in listofRoles)
            {
                var result = roleManager.Create(role);
                //////////
            }
        }
        #endregion
    }
}
