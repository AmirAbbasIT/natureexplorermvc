﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Database.ServerServices
{
    public class PictureServerService
    {
        private static PictureServerService obj;
        public static PictureServerService GetPictureServices()
        {
            if (obj == null)
            {
                obj = new PictureServerService();
            }
            return obj;
        }

        public void DeletePicture(string fullPath)
        {
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }
    }
}