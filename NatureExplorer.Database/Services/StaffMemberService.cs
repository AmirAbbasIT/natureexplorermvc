﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class StaffMemberService
    {
        private static StaffMemberService obj;
        public static StaffMemberService GetStaffServices()
        {
            if (obj == null)
            {
                obj = new StaffMemberService();
            }
            return obj;
        }

        #region GetAllStaffs
        public List<Staff> GetAllStaff()
        {
            using (var db = new NEContext())
            {
                return db.Staff.ToList();
            }
        }
 
        #endregion

        #region GetSingleStaff
        public Staff GetStaff(int ID)
        {
            var Staff = new Staff();
            using (var db = new NEContext())
            {
                Staff = db.Staff.Where(a => a.ID == ID).FirstOrDefault();
            }
            return Staff;
        }
        #endregion

        #region AddStaff
        public void AddStaff(Staff Staff, string pics)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            Staff.PictureID = picIDs.FirstOrDefault();
            if (Staff != null)
            {
                using (var db = new NEContext())
                {
                    var result = db.Staff.Add(Staff);
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region DeleteStaff
        public bool DeleteStaff(int ID)
        {
            using (var db = new NEContext())
            {
                var Staff = db.Staff.Where(a => a.ID == ID).SingleOrDefault();
                if (Staff != null)
                {
                    db.Staff.Remove(Staff);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditStaff
        public void EditStaff(Staff Staff, string pics)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            Staff.PictureID = picIDs.FirstOrDefault();
            using (var db = new NEContext())
            {
                var editedStaff = db.Staff.Where(a => a.ID == Staff.ID).SingleOrDefault();
                //foreach(var pic in editedStaff.Pictures)
                //{
                //    PictureServerService.GetPictureServices().DeletePicture(pic.Picture.URL);
                //}
                if (editedStaff != null)
                {
                    db.Entry(editedStaff).CurrentValues.SetValues(Staff);
                    db.SaveChanges();
                }
            }

        }
        #endregion


        public Picture GetStaffPictureByStaffID(int ID)
        {
            using (var db = new NEContext())
            {
                return db.Pictures.Where(a => a.ID == ID).FirstOrDefault();
            }
        }
  
     
    }
}
