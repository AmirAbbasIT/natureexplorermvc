﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NatureExplorer.Entities;
using Recommendations.Client;
using Recommendations.Client.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class ReccomedationService
    {
        private static ReccomedationService obj;
        public static ReccomedationService GetReccomendationServices()
        {
            if (obj == null)
            {
                obj = new ReccomedationService();
            }
            return obj;
        }

        //public void main()
        //{
        //    // modify the lines below based on the values you received when the site was configured.
        //    // note that the connection string is only needed by the sample because it uploads some sample
        //    // data to create a recommendation model.
        //    string recommendationsEndPointUri = ConfigurationService.GetConfigurationServices().ReccomendAppUrl();
        //    string apiAdminKey = ConfigurationService.GetConfigurationServices().ReccomendAdminKey();
        //    string connectionString = ConfigurationService.GetConfigurationServices().ReccomendStorageKey();

        //    // create recommendations client that allows us to work with the recommendations API
        //    var recommendationsClient = new RecommendationsAPI(new Uri(recommendationsEndPointUri));

        //    // add the api key header to all requests.
        //    // note that you can "train" recommendation models only with the modeling key,
        //    // you can score models with either the modeling key or the scoring key.
        //    recommendationsClient.HttpClient.DefaultRequestHeaders.Add("x-api-key", apiAdminKey);
        //    // comment out this line and use a pre-trained model Id if you would like to skip the training phase.
        //    Guid modelId = TrainModelUsingSampleData(recommendationsClient, connectionString,"");
        //    //Guid modelId = new Guid("3c0d2e0c-5950-42b4-89ca-78adef94f3d4");
        //    #region Scoring Example 1: Getting some recommendations for item with ID DHF-01159 

        //    string itemId = "385504209";
        //    Console.WriteLine($"Getting recommendations for item '{itemId}' using model '{modelId}':");
        //    IList<RecommendationResult> results = recommendationsClient.Models.GetItemRecommendations(modelId, itemId);
        //    PrintRecommendationResults(results);

        //    #endregion

        //    #region Scoring Example 2: Getting some recommendations from the default model

        //    // First, ensure this model is the default model.
        //    Console.WriteLine($"\t Setting model '{modelId}' as the default model");
        //    recommendationsClient.Models.SetDefaultModel(modelId);

        //    // Now that it is the default model, we can request recommendations using the default model
        //    Console.WriteLine($"Getting recommendations for item '{itemId}' using the default model:");
        //    results = recommendationsClient.Models.GetItemRecommendationsFromDefaultModel(itemId);
        //    PrintRecommendationResults(results);

        //    #endregion

        //    #region Scoring Example 3: Getting personalized recommendations

        //    // assuming some user had done two recent transactions -- she purchased item DAF-00448
        //    // on February 1st 2017, and purchased item DHF-01333 the previous day.
        //    var events = new List<UsageEvent>
        //    {
        //        new UsageEvent
        //        {
        //            ItemId = "6550649",
        //            EventType = EventType.Purchase,
        //            Timestamp = new DateTime(2017, 2, 1)
        //        },
        //        new UsageEvent
        //        {
        //            ItemId = "6550649",
        //            EventType = EventType.Purchase,
        //            Timestamp = new DateTime(2017, 1, 31)
        //        }
        //    };

        //    Console.WriteLine("Getting personalized recommendations for user with 2 transactions:");
        //    results = recommendationsClient.Models.GetPersonalizedRecommendationsFromDefaultModel(events);
        //    PrintRecommendationResults(results);

        //    #endregion

        //    Console.WriteLine();
        //    Console.WriteLine("Press any key to close application.");
        //    Console.ReadLine();
        //}

        public void SaveItemtoCatalog(string ID, string Name, String Catagory, String City,string path)
        {
            var csv = new StringBuilder();
            var line = String.Format("{0},{1},{2},,City={3}", ID, Name, Catagory, City);
            csv.AppendLine(line);
            using (StreamWriter writer = File.AppendText(Path.Combine(path, "TourCatalog.csv")))
            {
                writer.Write(csv.ToString());
            }
        }
        public void SaveEventtoUsage(string UserID, string ItemID, String Event,string path)
        {
            var csv = new StringBuilder();
            var line = String.Format("{0},{1},{2},{3}", UserID, ItemID, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), Event);
            csv.AppendLine(line);
            using(StreamWriter writer=File.AppendText(Path.Combine(path, "TourUsage.csv")))
            {
                writer.Write(csv.ToString());
                
            }
        }
        public void Train_SetDefaultModel(string ServerPath)
        {
            string recommendationsEndPointUri = ConfigurationService.GetConfigurationServices().ReccomendAppUrl();
            string apiAdminKey = ConfigurationService.GetConfigurationServices().ReccomendAdminKey();
            string connectionString = ConfigurationService.GetConfigurationServices().ReccomendStorageKey();
            ///
            var recommendationsClient = new RecommendationsAPI(new Uri(recommendationsEndPointUri));
            recommendationsClient.HttpClient.DefaultRequestHeaders.Add("x-api-key", apiAdminKey);
            ///
            Guid modelId = TrainModelUsingSampleData(recommendationsClient, connectionString,ServerPath);
            recommendationsClient.Models.SetDefaultModel(modelId);
        }

        public IList<RecommendationResult> GetItemtoItemRecomdendations(String itemId)
        {
            string recommendationsEndPointUri = ConfigurationService.GetConfigurationServices().ReccomendAppUrl();
            string apiAdminKey = ConfigurationService.GetConfigurationServices().ReccomendAdminKey();
            ///
            var recommendationsClient = new RecommendationsAPI(new Uri(recommendationsEndPointUri));
            recommendationsClient.HttpClient.DefaultRequestHeaders.Add("x-api-key", apiAdminKey);
            ///
            return recommendationsClient.Models.GetItemRecommendationsFromDefaultModel(itemId,3);
        }

        public IList<RecommendationResult> GetPersonalizedRecomdendations(List<UsageEvent> events)
        {
            string recommendationsEndPointUri = ConfigurationService.GetConfigurationServices().ReccomendAppUrl();
            string apiAdminKey = ConfigurationService.GetConfigurationServices().ReccomendAdminKey();
            ///
            var recommendationsClient = new RecommendationsAPI(new Uri(recommendationsEndPointUri));
            recommendationsClient.HttpClient.DefaultRequestHeaders.Add("x-api-key", apiAdminKey);
            ///
            //var events = new List<UsageEvent>
            //{
            //    new UsageEvent
            //    {
            //        ItemId = "6550649",
            //        EventType = EventType.Purchase,
            //        Timestamp = new DateTime(2017, 2, 1)
            //    },
            //    new UsageEvent
            //    {
            //        ItemId = "6550649",
            //        EventType = EventType.Purchase,
            //        Timestamp = new DateTime(2017, 1, 31)
            //    }
            //};

            return recommendationsClient.Models.GetPersonalizedRecommendationsFromDefaultModel(events,null,3);
        }

        static Guid TrainModelUsingSampleData(IRecommendationsAPI recommendationsClient, string storageAccountConnectionString,string Serverpath)
        {
            string sampleCatalogFileName = "TourCatalog.csv"; // the name of the sample catalog file that ships with this sample
            string sampleUsageEventsFileName = "TourUsage.csv"; // the name of the sample usage events file that ships with this sample
            string blobContainerName = "sample-data-cs"; // the name of the blob container we'll create to host our files
            string usageFolderName = "NatureExplorer-cs"; // the name of the relative blob folder that will host the usage event file(s)

            var storageAccount = CloudStorageAccount.Parse(storageAccountConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(blobContainerName);
            container.CreateIfNotExists();


            CloudBlockBlob catalogBlob = container.GetBlockBlobReference(sampleCatalogFileName);
            catalogBlob.UploadFromFile(Path.Combine(Serverpath, sampleCatalogFileName));

            //Console.WriteLine($"\t Uploading the sample usage events file ('{sampleUsageEventsFileName}') to a relative blob folder named '{usageFolderName}'");
            CloudBlockBlob usageEventsBlob = container.GetBlockBlobReference(Path.Combine(usageFolderName, sampleUsageEventsFileName));
            usageEventsBlob.UploadFromFile(Path.Combine(Serverpath, sampleUsageEventsFileName));

            // now that the catalog & usage event files were uploaded, we can trigger a new model training
            // Set the training parameters
            var modelParameters = new ModelParameters(
                description: "Sample created model",
                blobContainerName: blobContainerName,
                catalogFileRelativePath: sampleCatalogFileName, // the path to the uploaded catalog blob FILE 
                usageRelativePath: usageFolderName, // the path to the blob folder containing the usage file(s)
                evaluationUsageRelativePath: null, // we are not using evaluation usage events files in this model
                supportThreshold: 3,
                cooccurrenceUnit: CooccurrenceUnit.User,
                similarityFunction: SimilarityFunction.Jaccard,
                enableColdItemPlacement: true,
                enableColdToColdRecommendations: false,
                enableUserAffinity: true,
                allowSeedItemsInRecommendations: true,
                enableBackfilling: true,
                decayPeriodInDays: 30);

 
            Model model = recommendationsClient.Models.TrainNewModel(modelParameters);
            Guid modelId = model.Id.Value;
            do
            {
                Thread.Sleep(TimeSpan.FromSeconds(5));
                model = recommendationsClient.Models.GetModel(modelId);
                //Console.WriteLine($"\t Model '{modelId}' status: {model.ModelStatus} {model.ModelStatusMessage}");
            }
            while (model.ModelStatus != ModelStatus.Completed &&
                   model.ModelStatus != ModelStatus.Failed);
            return modelId;
        }

        static void PrintRecommendationResults(IEnumerable<RecommendationResult> results)
        {
            foreach (var result in results)
            {
                Console.WriteLine($"\t Got recommended item id: {result.RecommendedItemId} with score: {result.Score:F5}");
            }
        }

    }
}
