﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class SubscribersService
    {
        private static SubscribersService obj;
        public static SubscribersService GetSubscriberServices()
        {
            if (obj == null)
            {
                obj = new SubscribersService();
            }
            return obj;
        }

        #region Service
        public List<Subscribe> GetAllSubscribers()
        {
            using (var db = new NEContext())
            {
                return db.Subscribers.ToList();
            }
        }

        public void AddSubscriber(Subscribe sub)
        {
            using (var db = new NEContext())
            {
                db.Subscribers.Add(sub);
                db.SaveChanges();
            }
        }

        #endregion

    }
}
