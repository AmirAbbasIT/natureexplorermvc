﻿using NatureExplorer.Database.Managers;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class UserService
    {

        #region Singlton Pattern
        private static UserService instance;
        public static UserService GetUserServices()
        {
            if (instance == null)
            {
                instance = new UserService();
            }
            return instance;
        }
        #endregion

        public int UsersCount()
        {
            using(var db=new NEContext())
            {
                return db.Users.Count();
            }

        }
        public List<NEUser> GetUsers(string search,int pageNo,int pageSize)
        {
           
            var Users=new List<NEUser>();
            using(var context =new NEContext())
            {
               Users = context.Users.ToList();
            }
            if(!String.IsNullOrEmpty(search))
            {
                Users = Users.Where(U => U.Email.ToLower().Contains(search.ToLower())).Skip((pageNo-1)*pageSize).Take(pageSize).ToList();
            }
            else
            {
                Users = Users.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            }

            return Users;
        }

        public NEUser GetuserbyID(string ID)
        {
            using(var context=new NEContext())
            {
                return context.Users.Where(u => u.Id == ID).SingleOrDefault();
            }
        }

        public void UpdateUser(NEUser user)
        {
            using(var context=new NEContext())
            {
                var origionalUser = context.Users.Where(u => u.Id == user.Id).Single();
                origionalUser.FullName = user.FullName;
                origionalUser.Adress = user.Adress;
                origionalUser.ContantNo = user.PhoneNumber;
                origionalUser.PhoneNumber = user.PhoneNumber;
                context.Users.Attach(origionalUser);
                context.Entry(origionalUser).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
