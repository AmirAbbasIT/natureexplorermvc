﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class PaymentService
    {
        private static PaymentService obj;

        public static PaymentService GetPaymentServices()
        {
            if (obj == null)
            {
                obj = new PaymentService();
            }
            return obj;
        }


        #region EasyPaisa Region
        public EasyPaisa GetEasyPaisaTransection(int ID)
        {
            
            using(var db =new NEContext())
            {
                return db.EasyPisaPayments.Where(p => p.ID== ID).SingleOrDefault();
            }
        }
        public int AddEasyPaisaTransection(EasyPaisa Transection)
        {

            using (var db = new NEContext())
            {
                db.EasyPisaPayments.Add(Transection);
                db.SaveChanges();
            }
            return Transection.ID;
        }
        #endregion
    }
}
