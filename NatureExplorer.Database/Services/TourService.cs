﻿using NatureExplorer.Database.EntitiesForFE;
using NatureExplorer.Database.ServerServices;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class TourService
    {
        private static TourService obj;
        public static TourService GetTourServices()
        {
            if (obj == null)
            {
                obj = new TourService();
            }
            return obj;
        }
        #region Tours For Renewing
        public List<TourSelect> GetToursForSelectList()
        {
            using(var db=new NEContext())
            {
                return db.Tours.Select(t => new TourSelect{ID=t.ID,Title=t.Title}).ToList();
            }
        }
        #endregion

        #region TourCount
        public int TourCount(string search)
        {
            int count;
            using (var db = new NEContext())
            {
                if (string.IsNullOrEmpty(search))
                {
                    count = db.Tours.Count();
                }
                else
                {
                    count = db.Tours.Where(a => a.Title.ToLower().Contains(search.ToLower()) || a.Destination.ToLower().Contains(search.ToLower())).Count();
                }

            }
            return count;
        }
        #endregion

        #region TourNightDetail
        public List<TourNightDetail> GetTourNightDetail(int ID)
        {
            using (var db = new NEContext())
            {
                return db.TourNightDetails.Where(t => t.TourID == ID).ToList();
            }
        }
        #endregion

        #region GetAllTours

        public List<Tour> GetAllTours(int number)
        {
            using(var db =new NEContext())
            {
                return db.Tours.OrderByDescending(t=>t.DepartureDate).Take(number).Include(t => t.Pictures).Include(t => t.TourDays).Include(t => t.TourPrices).Include(t=>t.TourNights).Include(t=>t.TourBuses).ToList();
            }
        }
        public List<Tour> GetAllTours(string search, int pageNo, int pageSize)
        {
            var tours = new List<Tour>();
            using (var db = new NEContext())
            {
                tours = db.Tours.Include(t => t.Pictures).Include(t=>t.TourDays).Include(t=>t.TourPrices).Include(t=>t.TourFacilities).Include(t=>t.TourNights).Include(t => t.TourBuses).ToList();
            }
            if (!string.IsNullOrEmpty(search))
            {
                tours = tours.Where(a => a.Title.ToLower().Contains(search.ToLower()) || a.Destination.ToLower().Contains(search.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                tours = tours
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return tours;
        }
        public List<Tour> GetAllTours(string searchTerm, int? minimumPrice, int? maximumPrice, int? categoryID, int? sortBy, int pageNo, int pageSize)
        {
            var tours = new List<Tour>();
            using (var db = new NEContext())
            {
                tours = db.Tours.Include(t => t.Pictures).Include(t => t.TourDays).Include(t => t.TourPrices).Include(t=>t.TourFacilities).Include(t=>t.TourNights).Include(t => t.TourBuses).ToList();
            }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                tours = tours.Where(a => a.Title.ToLower().Contains(searchTerm.ToLower()) || a.Destination.ToLower().Contains(searchTerm.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                tours = tours
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return tours;
        }
       
        #endregion

        #region GetSingleTour
        public Tour GetTour(int ID)
        {
            var tour = new Tour();
            using (var db = new NEContext())
            {
                tour = db.Tours.Where(a => a.ID == ID).Include(a=>a.Pictures).Include(t=>t.TourDays).Include(t=>t.TourPrices).Include(t=>t.TourFacilities).Include(t=>t.TourNights).Include(t => t.TourBuses).FirstOrDefault();
            }
            return tour;
        }
        #endregion

        #region GetTourCities
        public List<String> GetTourCities()
        {
            using (var db = new NEContext())
            {
                return db.Tours.Select(t=>t.Destination).Distinct().ToList();
            }
        }
        #endregion

        #region AddTour
        public void AddTour(Tour tour, string pics,string ServerPath)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            tour.Pictures = new List<TourPicture>();
            tour.Pictures.AddRange(picIDs.Select(ID => new TourPicture() { PictureID = ID }));
            if (tour != null)
            {
                using (var db = new NEContext())
                {
                    db.Tours.Add(tour);
                    db.SaveChanges();
                    if (tour.TourBuses!=null && tour.TourBuses.Count!=0)
                    foreach(var tourBus in tour.TourBuses)
                    {
                        BusService.GetBusServices().AddTourBusSeats(tourBus);
                    }

                }
                NotificationService.GetNotificationServices().AddTourNotification(tour.Destination,tour.ID,tour.DepartureDate);
                ReccomedationService.GetReccomendationServices().SaveItemtoCatalog(tour.ID.ToString(), tour.Title, "Tour", tour.Destination,ServerPath);
            }
        }
        #endregion

        #region DeleteTour
        public bool DeleteTour(int ID)
        {
            using (var db = new NEContext())
            {
                var auction = db.Tours.Where(a => a.ID == ID).SingleOrDefault();
                if (auction != null)
                {
                    db.Tours.Remove(auction);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditTour
        public void EditTour(Tour tour, string pics)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            tour.Pictures = new List<TourPicture>();
            tour.Pictures.AddRange(picIDs.Select(ID => new TourPicture() { PictureID = ID,TourID=tour.ID }));

            if (tour.TourBuses != null)
                for (int i = 0; i < tour.TourBuses.Count; i++)
                {
                    tour.TourBuses[i].TourID = tour.ID;
                }

            if (tour.TourDays!=null)
            for(int i=0;i<tour.TourDays.Count;i++)
            {
                tour.TourDays[i].TourID = tour.ID;
            }
            if (tour.TourNights != null)
                for (int i = 0; i < tour.TourNights.Count; i++)
                {
                    tour.TourNights[i].TourID = tour.ID;
                }

            if (tour.TourPrices!=null)
            for (int i = 0; i < tour.TourPrices.Count; i++)
            {
                tour.TourPrices[i].TourID = tour.ID;
            }

            if(tour.TourFacilities!=null)
            for (int i = 0; i < tour.TourFacilities.Count; i++)
            {
                tour.TourFacilities[i].TourID= tour.ID;
            }
            using (var db = new NEContext())
            {
                var editedtour = db.Tours.Where(a => a.ID == tour.ID).SingleOrDefault();
                //foreach(var pic in editedtour.Pictures)
                //{
                //    PictureServerService.GetPictureServices().DeletePicture(pic.Picture.URL);
                //}
                if (editedtour != null)
                {
                    db.TourPictures.RemoveRange(editedtour.Pictures);
                    db.TourDayDetails.RemoveRange(editedtour.TourDays);
                    db.TourNightDetails.RemoveRange(editedtour.TourNights);
                    db.TourPricing.RemoveRange(editedtour.TourPrices);
                    db.TourFacilities.RemoveRange(editedtour.TourFacilities);
                    db.Entry(editedtour).CurrentValues.SetValues(tour);

                    if (tour.TourBuses != null)
                        db.TourBuses.AddRange(tour.TourBuses);
                    if (tour.Pictures!=null)
                    db.TourPictures.AddRange(tour.Pictures);
                    if(tour.TourDays!=null)
                    db.TourDayDetails.AddRange(tour.TourDays);
                    if (tour.TourNights != null)
                        db.TourNightDetails.AddRange(tour.TourNights);
                    if (tour.TourPrices!=null)
                    db.TourPricing.AddRange(tour.TourPrices);
                    if(tour.TourFacilities!=null)
                    db.TourFacilities.AddRange(tour.TourFacilities);
                    db.SaveChanges();
                }
            }
            if (tour.TourBuses != null && tour.TourBuses.Count != 0)
                foreach (var tourBus in tour.TourBuses)
                {
                    BusService.GetBusServices().AddTourBusSeats(tourBus);
                }

        }
        #endregion,

        #region GetTourPictures
        //This Will Return Pictures List
        public List<Picture> GetTourPictures(int ID)
        {
            using (var db = new NEContext())
            {
                var TourPictures = db.TourPictures.Where(a => a.TourID == ID).Select(p => p.PictureID).ToList();
                var Pictures = PictureService.GetPictureServices().GetPictures(TourPictures.ToArray());
                return Pictures;
            }
        }

        public Picture GetTourPicture(int ID)
        {
            using (var db = new NEContext())
            {
                var AuctionPictures = db.TourPictures.Where(a => a.TourID == ID).Select(p => p.PictureID).ToList();
                var Pictures = PictureService.GetPictureServices().GetPicture(AuctionPictures.ToArray());
                return Pictures;
            }
        }
        //This Will Return tour Picture List
        public List<TourPicture> GetTourPicturesByTourID(int ID)
        {
            using (var db = new NEContext())
            {
               return db.TourPictures.Where(a => a.TourID == ID).Include(p=>p.Picture).ToList();
            }
        }
        public List<String> GetTourPicturesURlByTourID(int ID)
        {
            var list = new List<String>();
            using (var db = new NEContext())
            {
                var pictures= db.TourPictures.Where(a => a.TourID == ID).Include(p => p.Picture).ToList();

                foreach(var pic in pictures)
                {
                    list.Add(pic.Picture.URL);
                }
            }
            return list;
        }
        #endregion

        #region Get Tour DaysDetail
        public List<TourDayDetail> GetToursDayDetail(int tourID)
        {
            using(var db=new NEContext())
            {
                return db.TourDayDetails.Where(t => t.TourID == tourID).ToList();
            }
        }
        #endregion

        #region Get Tour PricingDetail
        public List<TourPricing> GetToursPricingDetail(int tourID)
        {
            using (var db = new NEContext())
            {
                return db.TourPricing.Where(t => t.TourID == tourID).ToList();
            }
        }
        #endregion

        #region Get Tour Facilities
        public List<TourFacility> GetTourFacilities(int tourID)
        {
            using (var db = new NEContext())
            {
                return db.TourFacilities.Where(t => t.TourID == tourID).Include(f=>f.Facility).ToList();
            }
        }
        #endregion
    }
}
