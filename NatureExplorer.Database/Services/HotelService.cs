﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace NatureExplorer.Database.Services
{
    public class HotelService
    {
        private static HotelService obj;
        public static HotelService GetHotelServices()
        {
            if (obj == null)
            {
                obj = new HotelService();
            }
            return obj;
        }

        #region HotelCount
        public int HotelCount(string search)
        {
            int count;
            using (var db = new NEContext())
            {
                if (string.IsNullOrEmpty(search))
                {
                    count = db.Hotels.Count();
                }
                else
                {
                    count = db.Hotels.Where(a => a.Address.ToLower().Contains(search.ToLower())).Count();
                }

            }
            return count;
        }
        #endregion

        #region GetAllHotels
        public List<Hotel> GetAllHotels()
        {
            using (var db = new NEContext())
            {
                return db.Hotels.Include(h => h.Picture).Include(h => h.HotelFacilities).Include(h => h.HotelPackages).ToList();
            }
        }
        public List<string> GetHotelNames()
        {
            using (var db = new NEContext())
            {
                return db.Hotels.Select(h => h.Name).ToList();
            }
        }
        public List<Hotel> GetAllHotels(string search, int pageNo, int pageSize)
        {
            var hotels = new List<Hotel>();
            using (var db = new NEContext())
            {
                hotels = db.Hotels.Include(h => h.Picture).Include(h=>h.HotelFacilities).Include(h=>h.HotelPackages).ToList();
            }
            if (!string.IsNullOrEmpty(search))
            {
                hotels = hotels.Where(a => a.Name.ToLower().Contains(search.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                hotels = hotels
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return hotels;
        }
        public List<Hotel> GetAllHotels(string searchTerm, int? minimumPrice, int? maximumPrice, int? categoryID, int? sortBy, int pageNo,int pageSize)
        {
            var hotels = new List<Hotel>();
            using (var db = new NEContext())
            {
                hotels = db.Hotels.Include(h => h.Picture).Include(h => h.HotelFacilities).Include(h => h.HotelPackages).ToList();
            }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                hotels = hotels.Where(a => a.Address.ToLower().Contains(searchTerm.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                hotels = hotels
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return hotels;
        }
        #endregion

        #region GetSingleHotel
        public Hotel GetHotel(int ID)
        {
            var Hotel = new Hotel();
            using (var db = new NEContext())
            {
                Hotel = db.Hotels.Where(a => a.ID == ID).Include(h => h.Picture).Include(h => h.HotelFacilities).Include(h => h.HotelPackages).FirstOrDefault();
            }
            return Hotel;
        }
        #endregion

        #region AddHotel
        public void AddHotel(Hotel Hotel, string pics)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            Hotel.PictureID = picIDs.FirstOrDefault();
            if (Hotel != null)
            {
                using (var db = new NEContext())
                {
                    var result=db.Hotels.Add(Hotel);
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region DeleteHotel
        public bool DeleteHotel(int ID)
        {
            using (var db = new NEContext())
            {
                var hotel = db.Hotels.Where(a => a.ID == ID).SingleOrDefault();
                if (hotel != null)
                {
                    db.Hotels.Remove(hotel);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditHotel
        public void EditHotel(Hotel Hotel, string pics)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            Hotel.PictureID = picIDs.FirstOrDefault();
            if (Hotel.HotelFacilities != null)
                for (int i = 0; i < Hotel.HotelFacilities.Count; i++)
                {
                    Hotel.HotelFacilities[i].HotelID = Hotel.ID;
                }
            using (var db = new NEContext())
            {
                var editedHotel = db.Hotels.Where(a => a.ID == Hotel.ID).SingleOrDefault();
                //foreach(var pic in editedHotel.Pictures)
                //{
                //    PictureServerService.GetPictureServices().DeletePicture(pic.Picture.URL);
                //}
                if (editedHotel != null)
                {
                    db.HotelFacilities.RemoveRange(editedHotel.HotelFacilities);
                    db.Entry(editedHotel).CurrentValues.SetValues(Hotel);
                    if (Hotel.HotelFacilities != null)
                        db.HotelFacilities.AddRange(Hotel.HotelFacilities);
                    db.SaveChanges();
                }
            }

        }
        #endregion

        #region GetHotelPictures
        //This Will Return Pictures List
        //public List<Picture> GetHotelPictures(int ID)
        //{
        //    using (var db = new NEContext())
        //    {
        //        var HotelPictures = db.HotelPictures.Where(a => a.HotelID == ID).Select(p => p.PictureID).ToList();
        //        var Pictures = PictureService.GetPictureServices().GetPictures(HotelPictures.ToArray());
        //        return Pictures;
        //    }
        //}

        //public Picture GetHotelPicture(int ID)
        //{
        //    using (var db = new NEContext())
        //    {
        //        var AuctionPictures = db.HotelPictures.Where(a => a.HotelID == ID).Select(p => p.PictureID).ToList();
        //        var Pictures = PictureService.GetPictureServices().GetPicture(AuctionPictures.ToArray());
        //        return Pictures;
        //    }
        //}
        //This Will Return Hotel Picture List
        public Picture GetHotelPictureByHotelID(int ID)
        {
            using (var db = new NEContext())
            {
                return db.Pictures.Where(a => a.ID == ID).FirstOrDefault();
            }
        }
        #endregion

        #region Get Hotel Facilities
        public List<HotelFacility> GetHotelFacilities(int HotelID)
        {
            using (var db = new NEContext())
            {
                return db.HotelFacilities.Where(t => t.HotelID == HotelID).Include(f => f.Facility).ToList();
            }
        }
        #endregion

        #region Get Hotel Packages
        public List<HotelPackage> GetHotelPackages(int HotelID)
        {
            using (var db = new NEContext())
            {
                return db.HotelPackages.Where(t => t.HotelID == HotelID).Include(h=>h.Pictures).ToList();
            }
        }
        #endregion
    }
}
