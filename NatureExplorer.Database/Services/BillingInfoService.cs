﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class BillingInfoService
    {
        private static BillingInfoService obj;
        public static BillingInfoService GetBillingInfoServices()
        {
            if (obj == null)
            {
                obj = new BillingInfoService();
            }
            return obj;
        }

        
        #region GetAllBillingInfo
        public List<BillingInfo> GetAllBillingInfo()
        {
            var BillingInfo = new List<BillingInfo>();
            using (var db = new NEContext())
            {
                return db.BillingInfo.ToList();
            }
        }

        #endregion

        #region GetSingleBillingInfo
        public BillingInfo GetBillingInfo(int ID)
        {
            var BillingInfo = new BillingInfo();
            using (var db = new NEContext())
            {
                BillingInfo = db.BillingInfo.Where(q => q.ID == ID).FirstOrDefault();
            }
            return BillingInfo;
        }
        #endregion

        #region AddBillingInfo
        public void AddBillingInfo(BillingInfo BillingInfo)
        {

            if (BillingInfo != null)
            {
                using (var db = new NEContext())
                {
                    db.BillingInfo.Add(BillingInfo);
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region DeleteBillingInfo
        public bool DeleteBillingInfo(int ID)
        {
            using (var db = new NEContext())
            {
                var BillingInfo = db.BillingInfo.Where(a => a.ID == ID).SingleOrDefault();
                if (BillingInfo != null)
                {
                    db.BillingInfo.Remove(BillingInfo);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        #endregion
    }
}
