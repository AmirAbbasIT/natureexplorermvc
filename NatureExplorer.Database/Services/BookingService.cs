﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using static NatureExplorer.Entities.Enums;

namespace NatureExplorer.Database.Services
{
    public class BookingService
    {
        private static BookingService obj;
        public static BookingService GetBookingServices()
        {
            if (obj == null)
            {
                obj = new BookingService();
            }
            return obj;
        }
        #region Tour Booking
        #region BookingCount
        public int BookingCount(string search)
        {
            int count;
            using (var db = new NEContext())
            {
                if (string.IsNullOrEmpty(search))
                {
                    count = db.Bookings.Count();
                }
                else
                {
                    count = db.Bookings.Where(b => b.Date.ToString().Contains(search.ToLower())).Count();
                }

            }
            return count;
        }
        #endregion

        #region GetAllBookings
        public List<Booking> GetAllBookings(string search, int pageNo, int pageSize, int? Bookingtype)
        {
            var Bookings = new List<Booking>();
            int RecordID = -1;
            using (var db = new NEContext())
            {
                Bookings = db.Bookings.Include(b => b.User).Where(b => b.EntityType == Bookingtype).OrderByDescending(b => b.Date).ToList();
                if (!string.IsNullOrEmpty(search))
                {
                    if (Bookingtype == (int)BookingType.TourBooking)
                        RecordID = db.Tours.Where(t => t.Title.ToLower().Contains(search.ToLower())).Select(t => t.ID).SingleOrDefault();
                    else
                        RecordID = db.Hotels.Where(t => t.Name.ToLower().Contains(search.ToLower())).Select(t => t.ID).SingleOrDefault();

                }
            }
            if (!string.IsNullOrEmpty(search) && RecordID != -1)
            {
                Bookings = Bookings.Where(a => a.RecordID == RecordID)
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                Bookings = Bookings
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return Bookings;
        }

        public List<Booking> GetAllTourBookings(int TourID,string search, int pageNo, int pageSize, int? Bookingtype)
        {
            var Bookings = new List<Booking>();
            int RecordID = -1;
            using (var db = new NEContext())
            {
                Bookings = db.Bookings.Where(b=>b.RecordID==TourID).Include(b => b.User).Where(b => b.EntityType == Bookingtype).OrderByDescending(b => b.Date).ToList();
                if (!string.IsNullOrEmpty(search))
                {
                    if (Bookingtype == (int)BookingType.TourBooking)
                        RecordID = db.Tours.Where(t => t.Title.ToLower().Contains(search.ToLower())).Select(t => t.ID).SingleOrDefault();
                    else
                        RecordID = db.Hotels.Where(t => t.Name.ToLower().Contains(search.ToLower())).Select(t => t.ID).SingleOrDefault();

                }
            }
            if (!string.IsNullOrEmpty(search) && RecordID != -1)
            {
                Bookings = Bookings.Where(a => a.RecordID == RecordID)
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                Bookings = Bookings
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return Bookings;
        }
        #endregion

        #region GetSingleBooking
        public Booking GetBooking(int ID)
        {
            var Booking = new Booking();
            using (var db = new NEContext())
            {
                Booking = db.Bookings.Where(a => a.ID == ID).Include(a => a.User).FirstOrDefault();
            }
            return Booking;
        }
        #endregion
        #region GetUserBookings
        public List<Booking> GetUserBookings(string UserID)
        {
            using (var db = new NEContext())
            {
                return db.Bookings.Where(b => b.UserID == UserID).Include(b => b.User).ToList();
            }
        }
        #endregion
        #region AddBooking
        public int AddBooking(Booking Booking)
        {

            if (Booking != null)
            {
                using (var db = new NEContext())
                {
                    db.Bookings.Add(Booking);
                    db.SaveChanges();
                }
                return Booking.ID;
            }
            return -1; 
        }
        #endregion

        #region DeleteBooking
        public bool DeleteBooking(int ID)
        {
            using (var db = new NEContext())
            {
                var booking = db.Bookings.Where(a => a.ID == ID).SingleOrDefault();
                var Bus = booking.TourBusID;
                var user = booking.UserID;
                BusService.GetBusServices().FreeupSeats(user, Bus, booking.ID);
                if (booking != null)
                {
                    db.Bookings.Remove(booking);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditBooking
        public void EditBooking(Booking Booking)
        {
            using (var db = new NEContext())
            {
                var editedBooking = db.Bookings.Where(a => a.ID == Booking.ID).SingleOrDefault();
                if (editedBooking != null)
                {
                    db.Entry(editedBooking).CurrentValues.SetValues(Booking);
                    db.SaveChanges();
                }
            }
        }
        #endregion
        #endregion

        #region Hotel Booking
        #region BookingCount
        public int HotelBookingCount(string search)
        {
            int count;
            using (var db = new NEContext())
            {
                if (string.IsNullOrEmpty(search))
                {
                    count = db.RoomReservations.Count();
                }
                else
                {
                    count = db.RoomReservations.Where(b => b.CheckIn.ToString().Contains(search.ToLower())).Count();
                }

            }
            return count;
        }
        #endregion

        #region GetAllBookings
        public List<RoomReservation> GetAllHotelBookings(string search, int pageNo, int pageSize)
        {
            var Bookings = new List<RoomReservation>();
            using (var db = new NEContext())
            {
                Bookings = db.RoomReservations.Include(b => b.User).OrderByDescending(b => b.Date).ToList();
             
            }
            if (!string.IsNullOrEmpty(search))
            {
                Bookings = Bookings.Where(b => b.CheckIn.ToString().Contains(search.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                Bookings = Bookings
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return Bookings;
        }
        public List<RoomReservation> GetAllHotelBookingsByHotelID(int HotelID, int pageNo, int pageSize)
        {
            var Bookings = new List<RoomReservation>();
            using (var db = new NEContext())
            {
               return db.RoomReservations.Where(h=>h.HotelID==HotelID).Include(b => b.User).OrderByDescending(b => b.Date).ToList();

            }
            
        }
        #endregion

        #region GetSingleBooking
        public RoomReservation GetHotelBooking(int ID)
        {
            var Booking = new RoomReservation();
            using (var db = new NEContext())
            {
                Booking = db.RoomReservations.Where(a => a.ID == ID).Include(a => a.User).FirstOrDefault();
            }
            return Booking;
        }
        #endregion
        #region GetUserBookings
        public List<RoomReservation> GetUserHotelBookings(string UserID)
        {
            using (var db = new NEContext())
            {
                return db.RoomReservations.Where(b => b.UserID == UserID).Include(b => b.User).ToList();
            }
        }
        #endregion
        #region AddBooking
        public void AddHotelBooking(RoomReservation Booking)
        {

            if (Booking != null)
            {
                using (var db = new NEContext())
                {
                    db.RoomReservations.Add(Booking);
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region DeleteBooking
        public bool DeleteHotelBooking(int ID)
        {
            using (var db = new NEContext())
            {
                var booking = db.RoomReservations.Where(a => a.ID == ID).SingleOrDefault();
                if (booking != null)
                {
                    db.RoomReservations.Remove(booking);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditBooking
        public void EditHotelBooking(Booking Booking)
        {
            using (var db = new NEContext())
            {
                var editedBooking = db.Bookings.Where(a => a.ID == Booking.ID).SingleOrDefault();
                if (editedBooking != null)
                {
                    db.Entry(editedBooking).CurrentValues.SetValues(Booking);
                    db.SaveChanges();
                }
            }
        }
        #endregion
        #endregion

    }
}
