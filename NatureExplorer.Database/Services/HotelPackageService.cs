﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace NatureExplorer.Database.Services
{
    public class HotelPackageService
    {
        private static HotelPackageService obj;
        public static HotelPackageService GetHotelPackageServices()
        {
            if (obj == null)
            {
                obj = new HotelPackageService();
            }
            return obj;
        }
        #region HotelPackageCount
        public int HotelPackageCount(string search)
        {
            int count;
            using (var db = new NEContext())
            {
                if (string.IsNullOrEmpty(search))
                {
                    count = db.HotelPackages.Count();
                }
                else
                {
                    count = db.HotelPackages.Where(a => a.Hotel.Name.ToLower().Contains(search.ToLower())).Count();
                }

            }
            return count;
        }
        #endregion

        #region GetAllHotelPackages
        public List<HotelPackage> GetAllHotelPackages(string search, int pageNo, int pageSize)
        {
            var HotelPackages = new List<HotelPackage>();
            using (var db = new NEContext())
            {
                HotelPackages = db.HotelPackages.Include(t => t.Pictures).Include(h=>h.Hotel).ToList();
            }
            if (!string.IsNullOrEmpty(search))
            {
                HotelPackages = HotelPackages.Where(a => a.Hotel.Name.ToLower().Contains(search.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                HotelPackages = HotelPackages
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return HotelPackages;
        }
        #endregion

        #region GetSingleHotelPackage
        public HotelPackage GetHotelPackage(int ID)
        {
            var HotelPackage = new HotelPackage();
            using (var db = new NEContext())
            {
                HotelPackage = db.HotelPackages.Where(a => a.ID == ID).Include(a => a.Pictures).Include(h => h.Hotel).FirstOrDefault();
            }
            return HotelPackage;
        }
        #endregion

        #region AddHotelPackage
        public void AddHotelPackage(HotelPackage HotelPackage, string pics)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            HotelPackage.Pictures = new List<HotelPackagePicture>();
            HotelPackage.Pictures.AddRange(picIDs.Select(ID => new HotelPackagePicture() { PictureID = ID }));
            if (HotelPackage != null)
            {
                using (var db = new NEContext())
                {
                    db.HotelPackages.Add(HotelPackage);
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region DeleteHotelPackage
        public bool DeleteHotelPackage(int ID)
        {
            using (var db = new NEContext())
            {
                var auction = db.HotelPackages.Where(a => a.ID == ID).SingleOrDefault();
                if (auction != null)
                {
                    db.HotelPackages.Remove(auction);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditHotelPackage
        public void EditHotelPackage(HotelPackage HotelPackage, string pics)
        {
            var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
            HotelPackage.Pictures = new List<HotelPackagePicture>();
            HotelPackage.Pictures.AddRange(picIDs.Select(ID => new HotelPackagePicture() { PictureID = ID, HotelPackageID = HotelPackage.ID }));

            using (var db = new NEContext())
            {
                var editedHotelPackage = db.HotelPackages.Where(a => a.ID == HotelPackage.ID).SingleOrDefault();
                //foreach(var pic in editedHotelPackage.Pictures)
                //{
                //    PictureServerService.GetPictureServices().DeletePicture(pic.Picture.URL);
                //}
                if (editedHotelPackage != null)
                {
                    db.HotelPackagePictures.RemoveRange(editedHotelPackage.Pictures);
                    db.Entry(editedHotelPackage).CurrentValues.SetValues(HotelPackage);
                    db.Entry(editedHotelPackage).Property(u => u.HotelID).IsModified = false;

                    if (HotelPackage.Pictures != null)
                        db.HotelPackagePictures.AddRange(HotelPackage.Pictures);
                    db.SaveChanges();
                }
            }

        }
        #endregion

        #region GetHotelPackagePictures
        //This Will Return Pictures List
        public List<Picture> GetHotelPackagePictures(int ID)
        {
            using (var db = new NEContext())
            {
                var HotelPackagePictures = db.HotelPackagePictures.Where(a => a.HotelPackageID == ID).Select(p => p.PictureID).ToList();
                var Pictures = PictureService.GetPictureServices().GetPictures(HotelPackagePictures.ToArray());
                return Pictures;
            }
        }

        public Picture GetHotelPackagePicture(int ID)
        {
            using (var db = new NEContext())
            {
                var AuctionPictures = db.HotelPackagePictures.Where(a => a.HotelPackageID == ID).Select(p => p.PictureID).ToList();
                var Pictures = PictureService.GetPictureServices().GetPicture(AuctionPictures.ToArray());
                return Pictures;
            }
        }
        //This Will Return HotelPackage Picture List
        public List<HotelPackagePicture> GetHotelPackagePicturesByHotelPackageID(int ID)
        {
            using (var db = new NEContext())
            {
                return db.HotelPackagePictures.Where(a => a.HotelPackageID== ID).Include(p => p.Picture).ToList();
            }
        }
        #endregion

    }
}
