﻿using NatureExplorer.Database.EntitiesForFE;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class BusService
    {
        private static BusService obj;
        public static BusService GetBusServices()
        {
            if (obj == null)
            {
                obj = new BusService();
            }
            return obj;
        }
        #region GetReservationPercentages
        public double GetPercentage(int TourbusID)
        {
            using(var db=new NEContext())
            {
                double Booked = db.TourBusSeats.Where(st => st.TourBusID == TourbusID && st.Status == "Booked").Count();
                double Total = db.TourBusSeats.Where(st => st.TourBusID == TourbusID).Count();
                double percentage = ((Booked / Total) * 100);
                return percentage ;
            }
        }
        #endregion
        #region AddTourBus
        public void AddTourBusSeats(TourBus TourBus)
        {

            using (var db = new NEContext())
            {
                if(TourBus.Name=="HiAce")
                    for(int i=1;i<=18;i++)
                    {
                        var seat = new BusSeat();
                        seat.Status = "Available";
                        seat.SeatNo = i;
                        seat.TourBusID = TourBus.ID;
                        db.TourBusSeats.Add(seat);
                    }
                else if(TourBus.Name == "Coaster")
                    for (int i = 1; i <= 19; i++)
                    {
                        var seat = new BusSeat();
                        seat.Status = "Available";
                        seat.SeatNo = i;
                        seat.TourBusID = TourBus.ID;
                        db.TourBusSeats.Add(seat);
                    }
                db.SaveChanges();
            }
        }

        public List<TourBus> GetTourBuses(int TourID)
        {
            using(var db=new NEContext())
            {
                return db.TourBuses.Where(TB => TB.TourID == TourID).Include(TB=>TB.Seats).ToList();
            }
        }

        public TourBus GetTourBus(int BusID)
        {
            using (var db = new NEContext())
            {
                return db.TourBuses.Where(TB => TB.ID == BusID).Include(TB => TB.Seats).SingleOrDefault();
            }
        }
        public List<Seat> GetBusSeats(int TourBusID)
        {
            var list = new List<Seat>();
            using (var db = new NEContext())
            {
                list= db.TourBusSeats.Where(TB => TB.TourBusID == TourBusID).Select(TB=>new Seat{SeatNo=TB.SeatNo,Status=TB.Status }).ToList();
            }
            return list;
        }
        public List<BusSeat> GetUserBusSeats(int TourBusID,string UserID)
        {
            using (var db = new NEContext())
            {
                return db.TourBusSeats.Where(TB => TB.TourBusID == TourBusID && TB.UserID==UserID).Include(TB => TB.TourBus).ToList();
            }
        }
        public void BookSeats(string UserID,int tourBusID,int BookingID,int[] Seats)
        {
            using(var db=new NEContext())
            {
                var seats=db.TourBusSeats.Where(TB => TB.TourBusID == tourBusID && Seats.Contains(TB.SeatNo)).ToList();
                for(int i=0;i<seats.Count;i++)
                {
                    seats[i].Status = "Booked";
                    seats[i].UserID=UserID;
                    seats[i].BookingID = BookingID;
                    db.Entry(seats[i]).CurrentValues.SetValues(seats[i]);
                }
                db.SaveChanges();
            }
        }
        public void FreeupSeats(string UserID, int tourBusID, int BookingID)
        {
            using (var db = new NEContext())
            {
                var seats = db.TourBusSeats.Where(TB => TB.TourBusID == tourBusID && TB.UserID==UserID && TB.BookingID==BookingID).ToList();
                for (int i = 0; i < seats.Count; i++)
                {
                    db.TourBusSeats.Remove(seats[i]);
                }
                db.SaveChanges();
            }
        }
        public List<string> GetHotelNames()
        {
            using (var db = new NEContext())
            {
                return db.Hotels.Select(h => h.Name).ToList();
            }
        }


        public List<Hotel> GetAllHotels(string search, int pageNo, int pageSize)
        {
            var hotels = new List<Hotel>();
            using (var db = new NEContext())
            {
                hotels = db.Hotels.Include(h => h.Picture).Include(h => h.HotelFacilities).Include(h => h.HotelPackages).ToList();
            }
            if (!string.IsNullOrEmpty(search))
            {
                hotels = hotels.Where(a => a.Name.ToLower().Contains(search.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                hotels = hotels
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return hotels;
        }
        public List<Hotel> GetAllHotels(string searchTerm, int? minimumPrice, int? maximumPrice, int? categoryID, int? sortBy, int pageNo, int pageSize)
        {
            var hotels = new List<Hotel>();
            using (var db = new NEContext())
            {
                hotels = db.Hotels.Include(h => h.Picture).Include(h => h.HotelFacilities).Include(h => h.HotelPackages).ToList();
            }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                hotels = hotels.Where(a => a.Address.ToLower().Contains(searchTerm.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            }
            else
            {
                hotels = hotels
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return hotels;
        }
        #endregion

        #region GetSingleHotel
        //public Hotel GetHotel(int ID)
        //{
        //    var Hotel = new Hotel();
        //    using (var db = new NEContext())
        //    {
        //        Hotel = db.Hotels.Where(a => a.ID == ID).Include(h => h.Picture).Include(h => h.HotelFacilities).Include(h => h.HotelPackages).FirstOrDefault();
        //    }
        //    return Hotel;
        //}
        #endregion

        //#region AddHotel
        //public void AddHotel(Hotel Hotel, string pics)
        //{
        //    var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
        //    Hotel.PictureID = picIDs.FirstOrDefault();
        //    if (Hotel != null)
        //    {
        //        using (var db = new NEContext())
        //        {
        //            var result = db.Hotels.Add(Hotel);
        //            db.SaveChanges();
        //        }
        //    }
        //}
        //#endregion

        //#region DeleteHotel
        //public bool DeleteHotel(int ID)
        //{
        //    using (var db = new NEContext())
        //    {
        //        var hotel = db.Hotels.Where(a => a.ID == ID).SingleOrDefault();
        //        if (hotel != null)
        //        {
        //            db.Hotels.Remove(hotel);
        //            db.SaveChanges();
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //    }
        //}
        //#endregion

        //#region EditHotel
        //public void EditHotel(Hotel Hotel, string pics)
        //{
        //    var picIDs = pics.Split(',').Select(ID => int.Parse(ID)).ToList();
        //    Hotel.PictureID = picIDs.FirstOrDefault();
        //    if (Hotel.HotelFacilities != null)
        //        for (int i = 0; i < Hotel.HotelFacilities.Count; i++)
        //        {
        //            Hotel.HotelFacilities[i].HotelID = Hotel.ID;
        //        }
        //    using (var db = new NEContext())
        //    {
        //        var editedHotel = db.Hotels.Where(a => a.ID == Hotel.ID).SingleOrDefault();
        //        //foreach(var pic in editedHotel.Pictures)
        //        //{
        //        //    PictureServerService.GetPictureServices().DeletePicture(pic.Picture.URL);
        //        //}
        //        if (editedHotel != null)
        //        {
        //            db.HotelFacilities.RemoveRange(editedHotel.HotelFacilities);
        //            db.Entry(editedHotel).CurrentValues.SetValues(Hotel);
        //            if (Hotel.HotelFacilities != null)
        //                db.HotelFacilities.AddRange(Hotel.HotelFacilities);
        //            db.SaveChanges();
        //        }
        //    }

        //}
        //#endregion

        //#region GetHotelPictures
        ////This Will Return Pictures List
        ////public List<Picture> GetHotelPictures(int ID)
        ////{
        ////    using (var db = new NEContext())
        ////    {
        ////        var HotelPictures = db.HotelPictures.Where(a => a.HotelID == ID).Select(p => p.PictureID).ToList();
        ////        var Pictures = PictureService.GetPictureServices().GetPictures(HotelPictures.ToArray());
        ////        return Pictures;
        ////    }
        ////}

        ////public Picture GetHotelPicture(int ID)
        ////{
        ////    using (var db = new NEContext())
        ////    {
        ////        var AuctionPictures = db.HotelPictures.Where(a => a.HotelID == ID).Select(p => p.PictureID).ToList();
        ////        var Pictures = PictureService.GetPictureServices().GetPicture(AuctionPictures.ToArray());
        ////        return Pictures;
        ////    }
        ////}
        ////This Will Return Hotel Picture List
        //public Picture GetHotelPictureByHotelID(int ID)
        //{
        //    using (var db = new NEContext())
        //    {
        //        return db.Pictures.Where(a => a.ID == ID).FirstOrDefault();
        //    }
        //}
        //#endregion

        //#region Get Hotel Facilities
        //public List<HotelFacility> GetHotelFacilities(int HotelID)
        //{
        //    using (var db = new NEContext())
        //    {
        //        return db.HotelFacilities.Where(t => t.HotelID == HotelID).Include(f => f.Facility).ToList();
        //    }
        //}
        //#endregion

        //#region Get Hotel Packages
        //public List<HotelPackage> GetHotelPackages(int HotelID)
        //{
        //    using (var db = new NEContext())
        //    {
        //        return db.HotelPackages.Where(t => t.HotelID == HotelID).Include(h => h.Pictures).ToList();
        //    }
        //}
        //#endregion
        //}
    }
}
