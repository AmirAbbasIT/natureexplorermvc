﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class Querieservice
    {
        private static Querieservice obj;
        public static Querieservice GetQuerieservices()
        {
            if (obj == null)
            {
                obj = new Querieservice();
            }
            return obj;
        }

        #region QueryCount
        public int QueryCount(string search)
        {
            int count;
            using (var db = new NEContext())
            {
                if (string.IsNullOrEmpty(search))
                {
                    count = db.Queries.Count();
                }
                else
                {
                    count = db.Queries.Where(a => a.Email.ToLower().Contains(search.ToLower()) || a.Name.ToLower().Contains(search.ToLower())).Count();
                }

            }
            return count;
        }
        #endregion

        #region GetAllQueries

        public List<Query> GetAllQueries(string search, int pageNo, int pageSize)
        {
            var Queries = new List<Query>();
            using (var db = new NEContext())
            {
                Queries = db.Queries.OrderByDescending(o=>o.Date).ToList();
            }
            if (!string.IsNullOrEmpty(search))
            {
                Queries = Queries.Where(a => a.Email.ToLower().Contains(search.ToLower()) || a.Name.ToLower().Contains(search.ToLower()))
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();
            }
            else
            {
                Queries = Queries
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return Queries;
        }

        #endregion

        #region GetSingleQuery
        public Query GetQuery(int ID)
        {
            var Query = new Query();
            using (var db = new NEContext())
            {
                Query = db.Queries.Where(q=>q.ID==ID).FirstOrDefault();
            }
            return Query;
        }
        #endregion

        #region AddQuery
        public void AddQuery(Query Query)
        {
            
            if (Query != null)
            {
                using (var db = new NEContext())
                {
                    db.Queries.Add(Query);
                    db.SaveChanges();
                }   
            }
        }
        #endregion

        #region DeleteQuery
        public bool DeleteQuery(int ID)
        {
            using (var db = new NEContext())
            {
                var query = db.Queries.Where(a => a.ID == ID).SingleOrDefault();
                if (query != null)
                {
                    db.Queries.Remove(query);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        #endregion
    }
}
