﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class FacilityService
    {
        private static FacilityService obj;

        public static FacilityService GetFacilityServices()
        {
            if (obj == null)
            {
                obj = new FacilityService();
            }
            return obj;
        }

        #region GetAll Facilities
        public List<Facility> GetFacilities()
        {
            using (var db = new NEContext())
            {
                return db.Facilities.ToList();
            }
        }
        #endregion

        #region GetSingleFacility
        public Facility GetFacility(int ID)
        {
            var facility = new Facility();
            using (var db = new NEContext())
            {
                facility = db.Facilities.Where(a => a.ID == ID).FirstOrDefault();
            }
            return facility;
        }
        #endregion

        #region AddFacility
        public void AddFacility(Facility facility)
        {

            using (var db = new NEContext())
            {
                db.Facilities.Add(facility);
                db.SaveChanges();
            }

        }
        #endregion

        #region DeleteFacility
        public bool DeleteFacility(int ID)
        {
            using (var db = new NEContext())
            {
                var facility = db.Facilities.Where(a => a.ID == ID).SingleOrDefault();
                if (facility != null)
                {
                    db.Facilities.Remove(facility);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditFacility
        public void EditFacility(Facility facility)
        {
            using (var db = new NEContext())
            {
                var editedFacility = db.Facilities.Where(a => a.ID == facility.ID).SingleOrDefault();
                if (editedFacility != null)
                {
                    editedFacility.Name = facility.Name;
                    db.Facilities.Attach(editedFacility);
                    db.Entry(editedFacility).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

        }
        #endregion

    }
}
