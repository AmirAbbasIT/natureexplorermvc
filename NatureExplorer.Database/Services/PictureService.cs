﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class PictureService
    {
        private static PictureService obj;

        public static PictureService GetPictureServices()
        {
            if (obj == null)
            {
                obj = new PictureService();
            }
            return obj;
        }

        public int AddPicture(string url)
        {

            using (var db = new NEContext())
            {
                db.Pictures.Add(new Entities.Picture() { URL = url });
                db.SaveChanges();
                return Convert.ToInt32(db.Pictures.Where(p => p.URL == url).Select(p => p.ID).FirstOrDefault());
            }

        }
        public void DeletePicture(int ID)
        {
            using (var db = new NEContext())
            {
                var pic = db.Pictures.Where(p => p.ID == ID).SingleOrDefault();
                db.Pictures.Remove(pic);
                db.SaveChanges();

            }
        }
        public string FindPicture(int ID)
        {
            string url = "";
            using (var db = new NEContext())
            {
                url = db.Pictures.Where(p => p.ID == ID).Select(p => p.URL).First();
            }
            return url;
        }
        public List<Picture> GetPictures(int[] picIDs)
        {
            using (var db = new NEContext())
            {
                return db.Pictures.Where(p => picIDs.Contains(p.ID)).ToList();
            }
        }
        public Picture GetPicture(int[] picIDs)
        {
            using (var db = new NEContext())
            {
                return db.Pictures.Where(p => picIDs.Contains(p.ID)).First();
            }
        }
    }
}
