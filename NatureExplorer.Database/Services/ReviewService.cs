﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
    public class ReviewService
    {

        private static ReviewService obj;

        public static ReviewService GetReviewServices()
        {
            if (obj == null)
            {
                obj = new ReviewService();
            }
            return obj;
        }

        public int ReviewsCount()
        {
            using(var db=new NEContext())
            {
                return db.Reviews.Count();
            }
        }
        public List<Review> GetReviews(int EnityID, int RecordID)
        {

            using (var db = new NEContext())
            {
                return db.Reviews.Where(c => c.EntityID == EnityID && c.RecordID == RecordID).Include(m => m.User).ToList();

            }
        }

        public List<Review> GetUserReviews(string iD)
        {
            using (var db = new NEContext())
            {
                return db.Reviews.Where(r=>r.UserID==iD).Include(m => m.User).ToList();

            }
        }

        public List<Review> GetUserReviews(string userID, int EnityID)
        {

            using (var db = new NEContext())
            {
                return db.Reviews.Where(c => c.EntityID == EnityID && c.UserID == userID).Include(m => m.User).ToList();

            }
        }
        public void AddReview(Review Review)
        {
            if (Review != null)
            {
                using (var db = new NEContext())
                {
                    db.Reviews.Add(Review);
                    db.SaveChanges();
                }
            }
        }
    }
}
