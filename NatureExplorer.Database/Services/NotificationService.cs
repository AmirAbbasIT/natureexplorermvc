﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using static NatureExplorer.Entities.Enums;
using System.Configuration;

namespace NatureExplorer.Database.Services
{
    public class NotificationService
    {
        private static NotificationService obj;
        public static NotificationService GetNotificationServices()
        {
            if (obj == null)
            {
                obj = new NotificationService();
            }
            return obj;
        }

        public void AddTourNotification(string TourCity,int TourID,DateTime TourDate)
        {
            string notification = "A new Tour to " + TourCity + " on " + TourDate.ToString("dd/MMM/yyyy") + " has added";
            var notif = new Notification();
            notif.Message = notification;
            using (var db = new NEContext())
                notif.action = "Detail";
            notif.controller = "TourManage";
            notif.resourceID = TourID;
            notif.DateAdded = DateTime.Now;
            using(var db=new NEContext())
            {
                db.Notifications.Add(notif);
                var userIDs = db.Users.Select(u => u.Id).ToList();
                foreach(var UserID in userIDs)
                {
                    db.UserNotifications.Add(new UserNotification()
                    {
                        NotificationID = notif.ID,
                        UserID = UserID,
                        Status = (int)NotificationStatus.Unread
                    });
                }
                db.SaveChanges();
            }
        }

        public List<UserNotification> Get5Notifications(string UserID)
        {
            using(var db=new NEContext())
            {
                return db.UserNotifications.Where(n=>n.UserID==UserID).Include(n => n.Notification).Include(n => n.User).Take(5).ToList();
            }
        }

        public async Task SendEmail(string to,string subject,string message)
        {
            try
            {
                var senderEmail = ConfigurationSettings.AppSettings["AdminEmail"].ToString();
                var receiverEmail = to;
                var password = ConfigurationSettings.AppSettings["AdminEmailPass"].ToString();
                var sub = subject;
                var body = message;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail, password)
                };
                using (var mess = new MailMessage(senderEmail, receiverEmail)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    await smtp.SendMailAsync(mess);
                }
            }catch(Exception e)
            {

            }
            
        }

        public void SendSMS(string to,string message)
        {
            string MyUsername = "923404112872"; //Your Username At Sendpk.com 
            string MyPassword = "Amir12345"; //Your Password At Sendpk.com 
            string toNumber = to; //Recepient cell phone number with country code 
            string Masking = "Nature Explorer"; //Your Company Brand Name 
            string MessageText = message;
            SendSMSMethod(Masking, toNumber, MessageText, MyUsername, MyPassword);
        }
        public  string SendSMSMethod(string Masking, string toNumber, string MessageText, string MyUsername, string MyPassword)
        {
            String URI = "https://sendpk.com" +
            "/api/sms.php?" +
            "username=" + MyUsername +
            "&password=" + MyPassword +
            "&sender=" + Masking +
            "&mobile=" + toNumber +
            "&message=" + Uri.UnescapeDataString(MessageText); // Visual Studio 10-15 
            //"//&message=" + System.Net.WebUtility.UrlEncode(MessageText);// Visual Studio 12 
            try
            {
                WebRequest req = WebRequest.Create(URI);
                WebResponse resp = req.GetResponse();
                var sr = new System.IO.StreamReader(resp.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch (WebException ex)
            {
                var httpWebResponse = ex.Response as HttpWebResponse;
                if (httpWebResponse != null)
                {
                    switch (httpWebResponse.StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                            return "404:URL not found :" + URI;
                            break;
                        case HttpStatusCode.BadRequest:
                            return "400:Bad Request";
                            break;
                        default:
                            return httpWebResponse.StatusCode.ToString();
                    }
                }
            }
            return null;
        }
    }
}
