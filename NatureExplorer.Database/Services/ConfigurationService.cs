﻿using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Services
{
   public class ConfigurationService
    {
        private static ConfigurationService obj;
        public static ConfigurationService GetConfigurationServices()
        {
            if (obj == null)
            {
                obj = new ConfigurationService();
            }
            return obj;
        }

        #region GetConfigurations
        public Configuration GetAllConfiguration()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault();
            }
        }

        #endregion
        #region GetSingleConfiguration
        public Configuration GetConfiguration(int ID)
        {

            using (var db = new NEContext())
            {
                return db.Configurations.Where(a => a.ID == ID).FirstOrDefault();
            }

        }
        #endregion
        #region EditConfiguration
        public void EditConfiguration(Configuration Configuration)
        {

            using (var db = new NEContext())
            {
                var editedConfiguration = db.Configurations.Where(a => a.ID == Configuration.ID).SingleOrDefault();
                if (editedConfiguration != null)
                {
                    db.Entry(editedConfiguration).CurrentValues.SetValues(Configuration);
                    db.SaveChanges();
                }
            }

        }
        #endregion

        #region GetRespectiveCongiguration
        public string Email()
        {
            using(var db=new NEContext())
            {
                return db.Configurations.FirstOrDefault().Email;
            }
        }

        public string Password()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().Password;
            }
        }

        public string FaceBookAppID()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().FacebookAppID;
            }
        }
        public string FaceBookSecretID()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().FacebookSecretID;
            }
        }
        public string GoogleAppID()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().GoogleAppID;
            }
        }
        public string GoogleSecretID()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().GoogleSecretID;
            }
        }
        public string AuthorizeNetAppID()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().AuthorizeNetAppID;
            }
        }
        public string AuthorizeNetTransectionID()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().AuthorizeNetTransectionID;
            }
        }
        public string CompanyEmail()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().CompanyEmail;
            }
        }
        public string CompanyPhone()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().CompanyPhone;
            }
        }
        public string FaceBook()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().FaceBook;
            }
        }
        public string Instagram()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().Instagram;
            }
        }
        public string Twitter()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().Twitter;
            }
        }
        public string Contact()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().Contact;
            }
        }
        /// <summary>
        /// Rccomendation Configurations
        /// </summary>
        /// <returns></returns>
        public bool IsEnabledRecSys()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().ReccomendationSystem;
            }
        }

        public string ReccomendAppUrl()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().AppUrl;
            }
        }

        public string ReccomendAdminKey()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().AdminKey;
            }
        }
        public string ReccomendKey()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().ReccomendationKey;
            }
        }
        public string ReccomendStorageKey()
        {
            using (var db = new NEContext())
            {
                return db.Configurations.FirstOrDefault().StorageConnectionString;
            }
        }
        #endregion
    }
}
