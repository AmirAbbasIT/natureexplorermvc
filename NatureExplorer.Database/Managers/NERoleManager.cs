﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Managers
{
    public class NERoleManager : RoleManager<IdentityRole>
    {
            public NERoleManager(IRoleStore<IdentityRole> store)
                : base(store)
            {
            }

            public NERoleManager(IRoleStore<IdentityRole, string> store) : base(store)
            {
            }

            public static NERoleManager Create(IdentityFactoryOptions<NERoleManager> options, IOwinContext context)
            {
                return new NERoleManager(new RoleStore<IdentityRole>(context.Get<NEContext>()));

            }
  
    }
}
