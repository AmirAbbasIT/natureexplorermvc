﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using NatureExplorer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.Managers
{
    public class NESignInManager: SignInManager<NEUser, string>
    {

            public NESignInManager(NEUserManager userManager, IAuthenticationManager authenticationManager)
                : base(userManager, authenticationManager)
            {
            }

            public override Task<ClaimsIdentity> CreateUserIdentityAsync(NEUser user)
            {
                return user.GenerateUserIdentityAsync((NEUserManager)UserManager);
            }

            public static NESignInManager Create(IdentityFactoryOptions<NESignInManager> options, IOwinContext context)
            {
                return new NESignInManager(context.GetUserManager<NEUserManager>(), context.Authentication);
            }
        
    }

}

