﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NatureExplorer.Database.EntitiesForFE
{
    public class Seat
    {
        public int SeatNo { get; set; }
        public string Status { get; set; }
    }
}