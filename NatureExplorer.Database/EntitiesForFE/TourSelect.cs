﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureExplorer.Database.EntitiesForFE
{
    public class TourSelect
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }
}
